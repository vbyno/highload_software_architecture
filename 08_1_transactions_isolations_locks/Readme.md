# 08_1. Transactions, Isolations, Locks

## Assignment
Reproduce the main problems caused by transaction isolation levels for Percona's MySQL and PostgreSQL

Main Concurrency Problems:
- **Dirty read** – reading the data added by the transaction, which the database will rollback eventually.
- **Non-repeatable read** – not consistent reads. It arises when a transaction reads the same row twice but gets different data each time.
- **Phantom read** – similar to non-repeatable read, but it pays attention to the resulting scope of `SELECT` query, not only the one particular record. We can reproduce it when the parallel transaction inserts or deletes the row which has previously been displayed in another transaction's query.
- **Lost update** – when two transactions simultaneously update the same row, and the effect of one transaction is demolished.

## PostgreSQL
Setup
```sql
docker-compose exec postgres psql -U app
docker-compose exec postgres psql -U app -h postgres -d transactions_test
>
\l # show databases
CREATE DATABASE transactions_test;

\c transactions_test; # switch to the database
> You are now connected to database "transactions_test" as user "app".
```
Let's create a table and two records
```sql
CREATE TABLE accounts (
  name VARCHAR(10) NOT NULL CHECK (name <> ''),
  amount DECIMAL(2) NOT NULL
);

INSERT INTO accounts (name, amount) VALUES ('Joe', 10.0);
INSERT INTO accounts (name, amount) VALUES ('Jane', 20.0);

select * from accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20
(2 rows)
```
We are ready to test the isolation levels.

### Dirty read

Transaction 1
```sql
BEGIN;
UPDATE accounts SET amount = 20 WHERE name = 'Joe';
SELECT pg_sleep(15);
ROLLBACK;
```

Transaction 2
```sql
BEGIN;
SELECT pg_sleep(1);
SELECT amount from accounts WHERE name = 'Joe';
COMMIT;
```
### Read Uncommitted [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read uncommitted';

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
 amount
--------
     10
```

Amount is 10, not 20, therefore Transaction 2 did not read the data from uncommitted Transaction 1.

### Read Committed [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read committed';

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
 amount
--------
     10
```

### Repeatable Read [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'repeatable read';

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
 amount
--------
     10
```

### Serializable [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'serializable';

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
 amount
--------
     10
```

### Non-repeatable read

Transaction 1
```sql
UPDATE accounts SET amount = 10 where name = 'Joe';
BEGIN;
SELECT amount FROM accounts where name = 'Joe';
SELECT pg_sleep(15);
SELECT amount FROM accounts where name = 'Joe';
COMMIT;
UPDATE accounts SET amount = 10 where name = 'Joe';
```

Transaction 2
```sql
BEGIN;
SELECT pg_sleep(1);
UPDATE accounts set amount = amount + 1 where name = 'Joe';
COMMIT;
```


#### Read Uncommitted [REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read uncommitted';

--[...concurrent transactions execution....]

--First `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
--Second `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

As we can see, the second `SELECT` reads the updated value, hence the read is non-repeatable.


#### Read Committed [REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read committed';

--[...concurrent transactions execution....]

--First `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
--Second `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

The results are the same as for `read uncommitted`. Reproduced.


#### Repeatable Read [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'repeatable read';

--[...concurrent transactions execution....]

--First `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
--Second `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
```

The second statement output is the same as for the first one. Not reproduced.

#### Serializable [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'serializable';

--[...concurrent transactions execution....]

--First `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
--Second `SELECT` statements in Transaction 1:
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     10
```

The same as for `repeatable read`. Not reproduced.

### Phantom read

Transaction 1
```sql
BEGIN TRANSACTION;
SELECT * FROM accounts;
SELECT pg_sleep(15);
SELECT * FROM accounts;
COMMIT;
INSERT INTO accounts (name, amount) VALUES ('Jane', 20.0);
```

Transaction 2
```sql
BEGIN TRANSACTION;
SELECT pg_sleep(1);
DELETE from accounts WHERE name = 'Jane';
COMMIT;
```

#### Read Uncommitted [REPRODUCED]

```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read uncommitted';

--[...concurrent transactions execution....]

--[Transaction 1. First select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20

--[Transaction 1. Second select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
```

Jane's account is not displayed in the second query, which means the phantom read is reproduced.

#### Read Committed [REPRODUCED]

```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read committed';

--[...concurrent transactions execution....]

--[Transaction 1. First select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20

--[Transaction 1. Second select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
```

Phantom read is reproduced. The results are the same as for `read uncommitted`.

#### Repeatable Read [NOT REPRODUCED]

```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'repeatable read';

--[...concurrent transactions execution....]

--[Transaction 1. First select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20

--[Transaction 1. Second select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20
```

The second query results are the same as the first one, even though Jane's record was deleted in the concurrent transaction. Phantom read is not reproduced. The transaction reads the records from the snapshot.

#### Serializable [NOT REPRODUCED]

```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'serializable';

--[...concurrent transactions execution....]

--[Transaction 1. First select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20

--[Transaction 1. Second select statement output]
SELECT * FROM accounts;
 name | amount
------+--------
 Joe  |     10
 Jane |     20
```

The results are the same as for `repeatable read`. Not reproduced.

### Lost update V1

Transaction 1
```sql
UPDATE accounts SET amount = 10 where name = 'Joe';
BEGIN TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
UPDATE accounts SET amount = amount + 1 where name = 'Joe';
SELECT pg_sleep(15);
COMMIT;
```

Transaction 2
```sql
BEGIN TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT pg_sleep(1);
UPDATE accounts SET amount = amount + 1 where name = 'Joe';
COMMIT;
```

#### Read uncommitted [NOT REPRODUCED]
The result of post-update Select query is:
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read uncommitted';

--[...concurrent transactions execution....]

SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     12
```
Which means both amount updates were successful. No lost update.

#### Justification
It happens because the statement `UPDATE accounts SET amount = amount + 1 where name = 'Joe';` reads the `amount` from the row while it attempts to update it. But it can't update it till the concurrent transaction has not released the record lock. Within such an approach, we won't reproduce lost updates on any of the isolation levels.

### Lost update V2

Taking the above into account, we rewrite the queries to read the data in a separate statement and store it in a temp table.

Transaction 1
```sql
UPDATE accounts SET amount = 10 where name = 'Joe';
BEGIN TRANSACTION;
SELECT amount INTO current_amount FROM accounts where name = 'Joe' limit 1;
SELECT pg_sleep(15);
UPDATE accounts SET amount = (SELECT * from current_amount) + 1 where name = 'Joe';
DROP TABLE current_amount;
COMMIT;
```

Transaction 2
```sql
BEGIN TRANSACTION;
SELECT pg_sleep(1);
UPDATE accounts SET amount = amount + 1 where name = 'Joe';
COMMIT;
```

Let's try those transactions within different isolation levels:

#### Read Uncommitted [REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read uncommitted';

--[...concurrent transactions execution....]

SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

We've reproduced lost update. Two transactions added 1 to the initial amount, but the resulting amount is 11, not 12.

#### Read Committed [REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'read committed';

--[...concurrent transactions execution....]

SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

#### Repeatable Read [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'repeatable read';

--[...concurrent transactions execution....]

--[Transaction 1 output]
transactions_test=*# UPDATE accounts SET amount = (SELECT * from current_amount) + 1 where name = 'Joe';
ERROR:  could not serialize access due to concurrent update
transactions_test=!# DROP TABLE current_amount;
ERROR:  current transaction is aborted, commands ignored until end of transaction block
transactions_test=!# COMMIT;
ROLLBACK

--[result]
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

As we can see, the result is 11, but Transaction 1 was rolled back. If there were an application, we would get the error and re-tried the whole operation again, achieving the consistent state eventually.

#### Serializable [NOT REPRODUCED]
```sql
SET DEFAULT_TRANSACTION_ISOLATION TO 'serializable';

--[...concurrent transactions execution....]

--[Transaction 1 output]
transactions_test=*# UPDATE accounts SET amount = (SELECT * from current_amount) + 1 where name = 'Joe';
ERROR:  could not serialize access due to concurrent update
transactions_test=!# DROP TABLE current_amount;
ERROR:  current transaction is aborted, commands ignored until end of transaction block
transactions_test=!# COMMIT;
ROLLBACK

--[result]
SELECT amount FROM accounts where name = 'Joe';
 amount
--------
     11
```

Lost update is not reproduced, as for the `repeatable read` isolation level.

### PostgreSQL Summary
"+" – the problem is reproduced. "-" – it is not reproduced.
As we can see, disty read is not reproduced at all. Lost update can not be reproduced for repeatable read, everything else matches with the PostgreSQL documentation.

```bash
+------------------+-------+------------+---------+--------+
| Isolation        | Dirty | Non-repea  | Phantom | Lost   |
|   level          | reads | table read | read    | update |
+------------------+-------+------------+---------+--------+
| READ UNCOMMITTED |   -   |     +      |    +    |   +    |
+------------------+-------+------------+---------+--------+
| READ COMMITTED   |   -   |     +      |    +    |   +    |
+------------------+-------+------------+---------+--------+
| REPEATABLE READ  |   -   |     -      |    -    |   -    |
+------------------+-------+------------+---------+--------+
| SERIALIZABLE     |   -   |     -      |    -    |   -    |
+------------------+-------+------------+---------+--------+
```

## Percona MySQL

```sql
docker-compose exec percona_mysql mysql -u root -p
CREATE DATABASE transactions_test;
USE transactions_test;

-- Let's create a table and two records
CREATE TABLE accounts (
  name VARCHAR(10) NOT NULL CHECK (name <> ''),
  amount DECIMAL(2) NOT NULL
);

INSERT INTO accounts (name, amount) VALUES ('Joe', 10.0);
INSERT INTO accounts (name, amount) VALUES ('Jane', 20.0);

select * from accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
2 rows in set (0.01 sec)
```
### Dirty read

Transaction 1
```sql
START TRANSACTION;
UPDATE accounts SET amount = 20 WHERE name = 'Joe';
DO sleep(15);
ROLLBACK;
```

Transaction 2
```sql
START TRANSACTION;
DO sleep(1);
SELECT amount from accounts WHERE name = 'Joe';
COMMIT;
```
### Read Uncommitted [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT @@global.transaction_ISOLATION;

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
+--------+
| amount |
+--------+
|     20 |
+--------+
```

The amount is 20, which means transaction #2 accessed the data set by rolled-back transaction. Dirty read reprduced.

### Read Committed [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
```

Dirty read is not reproduced.

### Repeatable Read [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;

--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
```
Not reproduced. Select query of transaction 2 waits for transaction 1 to release the lock.

### Serializable [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SELECT @@transaction_ISOLATION;
--[...concurrent transactions execution....]

SELECT amount from accounts WHERE name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
```

Not reproduced. Unlike `read committed` and `repeatable read`, select query of transaction 2 waits for transaction 1 to release the lock.


### Non-repeatable read

Transaction 1
```sql
UPDATE accounts SET amount = 10 where name = 'Joe';
BEGIN;
SELECT amount FROM accounts where name = 'Joe';
DO sleep(15);
SELECT amount FROM accounts where name = 'Joe';
COMMIT;
UPDATE accounts SET amount = 10 where name = 'Joe';
```

Transaction 2
```sql
BEGIN;
DO sleep(1);
UPDATE accounts set amount = amount + 1 where name = 'Joe';
COMMIT;
```

#### Read Uncommitted [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
The second statement returns the updated value for `amount`. Reproduced.


#### Read Committed [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
The results are the same as for `read uncommitted`. Reproduced.


#### Repeatable Read [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
```
Not reproduced. The second statement returns the same result.


#### Serializable [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     10 |
+--------+
```
Not reproduced. Unlike the previous isolation levels, second transaction waits for the first transaction to unlock the row.


### Phantom read

Transaction 1
```sql
START TRANSACTION;
SELECT * FROM accounts;
DO sleep(15);
SELECT * FROM accounts;
COMMIT;
INSERT INTO accounts (name, amount) VALUES ('Jane', 20.0);
```

Transaction 2
```sql
START TRANSACTION;
DO sleep(1);
DELETE from accounts WHERE name = 'Jane';
COMMIT;
```

I've tried to reproduce Phantorm read as it is reproduced [here](https://www.percona.com/blog/2021/02/11/various-types-of-innodb-transaction-isolation-levels-explained-using-terminal/#crayon-61ebc84a61206555696408), but had no luck:

<details>
  <summary>Click to expand</summary>

  ```sql
  alter table accounts add primary key(name);

  desc accounts;
  +--------+--------------+------+-----+---------+-------+
  | Field  | Type         | Null | Key | Default | Extra |
  +--------+--------------+------+-----+---------+-------+
  | name   | varchar(10)  | NO   | PRI | NULL    |       |
  | amount | decimal(2,0) | NO   |     | NULL    |       |
  +--------+--------------+------+-----+---------+-------+
  ```

  Transaction 1
  ```sql
  DELETE from accounts WHERE name = 'Kate';
  START TRANSACTION;
  SELECT * FROM accounts;
  DO sleep(15);
  SELECT * FROM accounts;
  UPDATE accounts SET amount=0.1 WHERE name='Kate';
  SELECT * FROM accounts;
  COMMIT;
  ```
  <!-- INSERT INTO accounts (name, amount) VALUES ('Jane', 20.0); -->

  Transaction 2
  ```sql
  START TRANSACTION;
  DO sleep(1);
  INSERT INTO accounts (name, amount) VALUES ('Kate', 0.0);
  COMMIT;
  ```

  In the original article, this is the way to reproduce phantom read for repeatable read, but in my case the transaction 1 dis not change the row inserted by the concurrent transaction. Therefore, I recieve the same output for each select * query.
  ```sql
  DELETE from accounts WHERE name = 'Kate';
  Query OK, 1 row affected (0.01 sec)
  ```
  ```sql
  ALTER TABLE accounts DROP PRIMARY KEY;
  ```
</details>

#### Read Uncommitted [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
+------+--------+
```
Reproduced. The second query does not display `Jane` record.

#### Read Committed [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
+------+--------+
```
Reproduced.

#### Repeatable Read [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
```
Not Reproduced. The second query returns the same results.

#### Serializable [NOT REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
--[...concurrent transactions execution....]
-- Transaction 1 queries:
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
SELECT * FROM accounts;
+------+--------+
| name | amount |
+------+--------+
| Joe  |     10 |
| Jane |     20 |
+------+--------+
```
Not Reproduced. The second transaction waits for the first one to complete.

### Lost update

Transaction 1
```sql
UPDATE accounts SET amount = 10 where name = 'Joe';
START TRANSACTION;
SELECT amount INTO @current_amount FROM accounts where name = 'Joe' limit 1;
DO sleep(15);
UPDATE accounts SET amount = (SELECT @current_amount) + 1 where name = 'Joe';
COMMIT;
```

Transaction 2
```sql
START TRANSACTION;
DO sleep(1);
UPDATE accounts SET amount = amount + 1 where name = 'Joe';
COMMIT;
```

#### Read Uncommitted [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--[...concurrent transactions execution....]
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
The amount is 11, despite the fact both transaction that added +1 to the initial value (10) were successful. Meaning, we have reproduced lost update.


#### Read Committed [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
--[...concurrent transactions execution....]
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
The amount is 11. Reproduced.

#### Repeatable Read [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
--[...concurrent transactions execution....]
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
Reproduced again.

#### Serializable [REPRODUCED]
```sql
SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
--[...concurrent transactions execution....]
SELECT amount FROM accounts where name = 'Joe';
+--------+
| amount |
+--------+
|     11 |
+--------+
```
Reproduced, even though the second transaction was waiting for the first one to complete.

### Percona MySQL Summary
"+" – the problem is reproduced. "-" – it is not reproduced.
Lost update is reproduced everywhere. Could not reproduce Phantom read for `Repeatable READ` isolation level. Everything else as expected.

```bash
+------------------+-------+------------+---------+--------+
| Isolation        | Dirty | Non-repea  | Phantom | Lost   |
|   level          | reads | table read | read    | update |
+------------------+-------+------------+---------+--------+
| READ UNCOMMITTED |   +   |     +      |    +    |   +    |
+------------------+-------+------------+---------+--------+
| READ COMMITTED   |   -   |     +      |    +    |   +    |
+------------------+-------+------------+---------+--------+
| REPEATABLE READ  |   -   |     -      |    -    |   +    |
+------------------+-------+------------+---------+--------+
| SERIALIZABLE     |   -   |     -      |    -    |   +    |
+------------------+-------+------------+---------+--------+
```

## References
- [Transaction isolation levels](https://ru.wikipedia.org/wiki/%D0%A3%D1%80%D0%BE%D0%B2%D0%B5%D0%BD%D1%8C_%D0%B8%D0%B7%D0%BE%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%82%D1%80%D0%B0%D0%BD%D0%B7%D0%B0%D0%BA%D1%86%D0%B8%D0%B9)
- [PostgreSQL Transaction Isolation](https://www.postgresql.org/docs/9.5/transaction-iso.html)
- [Non-repeatable read vs Phantom read](https://stackoverflow.com/questions/11043712/what-is-the-difference-between-non-repeatable-read-and-phantom-read)
- [Postgres row/table locks](https://www.postgresql.org/docs/9.1/explicit-locking.html)
- [The Internals of PostgreSQL](https://www.interdb.jp/pg/pgsql05.html)
- [Various Types of InnoDB Transaction Isolation Levels Explained Using Terminal](https://www.percona.com/blog/2021/02/11/various-types-of-innodb-transaction-isolation-levels-explained-using-terminal/)

## Questions:
1. How to reproduce Lost Update for PostgreSQL Repeatable Read?
1. How to reproduce Phantom Read for Percona's Repeatable Read?
