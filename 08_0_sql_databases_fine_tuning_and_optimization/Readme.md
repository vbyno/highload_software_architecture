# 08_0. SQL Databases. Fine Tuning and Optimization

## Homework
MySQL with InnoDB

1. Make table on 40М user records
2. Compare read performance on date of birth:
a) with no index
b) with BTREE index
c) wtth HASH index
3. Compare write performance with different `innodb_flush_log_at_trx_commit` values and different RPS


## 1. Make table on 40М user records

```bash
docker-compose up
```
Visit http://localhost:8080/ and input:
  Username: app
  Password: pass
and choose `db_highload` database

Execute these queries:
```sql
use db_highload;
drop table if exists users;

create table users(
   id INT NOT NULL AUTO_INCREMENT,
   full_name VARCHAR(100) NOT NULL,
   birth_date DATE NOT NULL,
   PRIMARY KEY ( id )
);

DROP PROCEDURE IF EXISTS __main__;
DELIMITER $
CREATE PROCEDURE __main__(IN max_count INT)
BEGIN
	SET @counter = 1;
    WHILE(@counter <= max_count)
    DO
    	INSERT INTO users (full_name, birth_date)
        VALUES ( concat("Volodymyr", @counter), (SELECT DATE_SUB(NOW(), INTERVAL @counter day)));
      SET @counter = @counter + 1;
    END WHILE;
END;
$
DELIMITER ;

CALL __main__(100000);
```

Then execute this query several times while the number of records is >= 40M
```sql
INSERT INTO users (full_name, birth_date) SELECT full_name, birth_date from users limit 5000000;
select count(*) from users;
```

## 2 Compare read performance on date of birth:
### a) With no index
mysql -u app -h mysql -p

```sql
select count(*) from users where birth_date > '2020-11-10';
+----------+
| count(*) |
+----------+
|   150215 |
+----------+
1 row in set (20.59 sec)

explain select count(*) from users where birth_date > '2020-11-10';
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra       |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
|  1 | SIMPLE      | users | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39932094 |    33.33 | Using where |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+


select count(*) from users where birth_date = '1950-11-10';
+----------+
| count(*) |
+----------+
|      401 |
+----------+
1 row in set (19.85 sec)

explain select count(*) from users where birth_date = '1950-11-10';
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra       |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
|  1 | SIMPLE      | users | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39932094 |    10.00 | Using where |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+


select count(*) from users where birth_date < '1950-01-10';
+----------+
| count(*) |
+----------+
| 29466156 |
+----------+
1 row in set (20.58 sec)

explain select count(*) from users where birth_date < '1950-01-10';
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra       |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+
|  1 | SIMPLE      | users | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39932094 |    33.33 | Using where |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------------+


select min(birth_date) from users;
+-----------------+
| min(birth_date) |
+-----------------+
| 1748-02-04      |
+-----------------+
1 row in set (20.23 sec)

explain select min(birth_date) from users;
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------+
|  1 | SIMPLE      | users | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39932094 |   100.00 | NULL  |
+----+-------------+-------+------------+------+---------------+------+---------+------+----------+----------+-------+
```

### b) With BTREE index
```sql
CREATE INDEX birth_date_btree_index USING BTREE ON users (birth_date);
Query OK, 0 rows affected (2 min 34.03 sec)

select count(*) from users where birth_date > '2020-11-10';
+----------+
| count(*) |
+----------+
|   150215 |
+----------+
1 row in set (0.06 sec)

explain select count(*) from users where birth_date > '2020-11-10';
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+--------+----------+--------------------------+
| id | select_type | table | partitions | type  | possible_keys          | key                    | key_len | ref  | rows   | filtered | Extra                    |
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+--------+----------+--------------------------+
|  1 | SIMPLE      | users | NULL       | range | birth_date_btree_index | birth_date_btree_index | 3       | NULL | 285522 |   100.00 | Using where; Using index |
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+--------+----------+--------------------------+


select count(*) from users where birth_date = '1950-11-10';
+----------+
| count(*) |
+----------+
|      401 |
+----------+
1 row in set (0.00 sec)

explain select count(*) from users where birth_date = '1950-11-10';
+----+-------------+-------+------------+------+------------------------+------------------------+---------+-------+------+----------+-------------+
| id | select_type | table | partitions | type | possible_keys          | key                    | key_len | ref   | rows | filtered | Extra       |
+----+-------------+-------+------------+------+------------------------+------------------------+---------+-------+------+----------+-------------+
|  1 | SIMPLE      | users | NULL       | ref  | birth_date_btree_index | birth_date_btree_index | 3       | const |  401 |   100.00 | Using index |
+----+-------------+-------+------------+------+------------------------+------------------------+---------+-------+------+----------+-------------+


select count(*) from users where birth_date < '1950-01-10';
+----------+
| count(*) |
+----------+
| 29466156 |
+----------+
1 row in set (12.90 sec)

explain select count(*) from users where birth_date < '1950-01-10';
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+----------+----------+--------------------------+
| id | select_type | table | partitions | type  | possible_keys          | key                    | key_len | ref  | rows     | filtered | Extra                    |
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+----------+----------+--------------------------+
|  1 | SIMPLE      | users | NULL       | range | birth_date_btree_index | birth_date_btree_index | 3       | NULL | 19858668 |   100.00 | Using where; Using index |
+----+-------------+-------+------------+-------+------------------------+------------------------+---------+------+----------+----------+--------------------------+

select min(birth_date) from users;
+-----------------+
| min(birth_date) |
+-----------------+
| 1748-02-04      |
+-----------------+
1 row in set (0.00 sec)

explain select min(birth_date) from users;
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra                        |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+
|  1 | SIMPLE      | NULL  | NULL       | NULL | NULL          | NULL | NULL    | NULL | NULL |     NULL | Select tables optimized away |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+

DROP INDEX birth_date_btree_index ON users;
```

### c) With HASH index
```sql
CREATE INDEX birth_date_hash_index USING HASH ON users (birth_date);
Query OK, 0 rows affected, 1 warning (1 min 57.05 sec)

select count(*) from users where birth_date > '2020-11-10';
+----------+
| count(*) |
+----------+
|   150215 |
+----------+
1 row in set (0.08 sec)

explain select count(*) from users where birth_date > '2020-11-10';
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+--------+----------+--------------------------+
| id | select_type | table | partitions | type  | possible_keys         | key                   | key_len | ref  | rows   | filtered | Extra                    |
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+--------+----------+--------------------------+
|  1 | SIMPLE      | users | NULL       | range | birth_date_hash_index | birth_date_hash_index | 3       | NULL | 285522 |   100.00 | Using where; Using index |
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+--------+----------+--------------------------+


select count(*) from users where birth_date = '1950-11-10';
+----------+
| count(*) |
+----------+
|      401 |
+----------+
1 row in set (0.01 sec)

explain select count(*) from users where birth_date = '1950-11-10';
+----+-------------+-------+------------+------+-----------------------+-----------------------+---------+-------+------+----------+-------------+
| id | select_type | table | partitions | type | possible_keys         | key                   | key_len | ref   | rows | filtered | Extra       |
+----+-------------+-------+------------+------+-----------------------+-----------------------+---------+-------+------+----------+-------------+
|  1 | SIMPLE      | users | NULL       | ref  | birth_date_hash_index | birth_date_hash_index | 3       | const |  401 |   100.00 | Using index |
+----+-------------+-------+------------+------+-----------------------+-----------------------+---------+-------+------+----------+-------------+


select count(*) from users where birth_date < '1950-01-10';
+----------+
| count(*) |
+----------+
| 29466156 |
+----------+
1 row in set (13.93 sec)

explain select count(*) from users where birth_date < '1950-01-10';
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+----------+----------+--------------------------+
| id | select_type | table | partitions | type  | possible_keys         | key                   | key_len | ref  | rows     | filtered | Extra                    |
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+----------+----------+--------------------------+
|  1 | SIMPLE      | users | NULL       | range | birth_date_hash_index | birth_date_hash_index | 3       | NULL | 19858668 |   100.00 | Using where; Using index |
+----+-------------+-------+------------+-------+-----------------------+-----------------------+---------+------+----------+----------+--------------------------+


select min(birth_date) from users;
+-----------------+
| min(birth_date) |
+-----------------+
| 1748-02-04      |
+-----------------+
1 row in set (0.00 sec)

explain select min(birth_date) from users;
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra                        |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+
|  1 | SIMPLE      | NULL  | NULL       | NULL | NULL          | NULL | NULL    | NULL | NULL |     NULL | Select tables optimized away |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+------------------------------+


DROP INDEX birth_date_hash_index ON users;
```

## 3. Compare write performance with different `innodb_flush_log_at_trx_commit` values and different RPS

1. `docker-compose down` for current container
2. `cd ../shared/blog && docker-compose up`
3. Run queries in multiple concurrency
siege -R 04_stress_testing/siege.conf -t30s -i -c 10 http://localhost/users.json

### The details of the experiments:

<details>
  <summary>innodb_flush_log_at_trx_commit=0</summary>

    10
    Transactions:                    784 hits
    Availability:                 100.00 %
    Elapsed time:                  29.54 secs
    Data transferred:               0.54 MB
    Response time:                  0.37 secs
    Transaction rate:              26.54 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                    9.94
    Successful transactions:         784
    Failed transactions:               0
    Longest transaction:            1.41
    Shortest transaction:           0.19
    50
    Transactions:                    798 hits
    Availability:                 100.00 %
    Elapsed time:                  29.56 secs
    Data transferred:               0.55 MB
    Response time:                  1.76 secs
    Transaction rate:              27.00 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                   47.59
    Successful transactions:         798
    Failed transactions:               0
    Longest transaction:            6.37
    Shortest transaction:           0.22
    100
    Transactions:                    736 hits
    Availability:                 100.00 %
    Elapsed time:                  29.26 secs
    Data transferred:               0.51 MB
    Response time:                  3.59 secs
    Transaction rate:              25.15 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                   90.41
    Successful transactions:         736
    Failed transactions:               0
    Longest transaction:           13.21
    Shortest transaction:           0.18
    200
    Transactions:                    661 hits
    Availability:                 100.00 %
    Elapsed time:                  29.22 secs
    Data transferred:               0.45 MB
    Response time:                  6.91 secs
    Transaction rate:              22.62 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                  156.37
    Successful transactions:         661
    Failed transactions:               0
    Longest transaction:           27.34
    Shortest transaction:           0.27
    500
    [error] descriptor table full sock.c:402: Too many open files
    Transactions:                    247 hits
    Availability:                  16.26 %
    Elapsed time:                  35.17 secs
    Data transferred:               0.17 MB
    Response time:                  5.95 secs
    Transaction rate:               7.02 trans/sec
    Throughput:                     0.00 MB/sec
    Concurrency:                   41.76
    Successful transactions:         247
    Failed transactions:            1272
    Longest transaction:            8.52
    Shortest transaction:           0.00

</details>

<details>
  <summary>innodb_flush_log_at_trx_commit=1</summary>

    10
    Transactions:                    712 hits
    Availability:                 100.00 %
    Elapsed time:                  29.63 secs
    Data transferred:               0.49 MB
    Response time:                  0.41 secs
    Transaction rate:              24.03 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                    9.92
    Successful transactions:         712
    Failed transactions:               0
    Longest transaction:            1.51
    Shortest transaction:           0.24
    50
    Transactions:                    181 hits
    Availability:                 100.00 %
    Elapsed time:                  29.24 secs
    Data transferred:               0.12 MB
    Response time:                  7.03 secs
    Transaction rate:               6.19 trans/sec
    Throughput:                     0.00 MB/sec
    Concurrency:                   43.52
    Successful transactions:         181
    Failed transactions:               0
    Longest transaction:            9.60
    Shortest transaction:           4.51
    100
    Transactions:                    696 hits
    Availability:                 100.00 %
    Elapsed time:                  29.38 secs
    Data transferred:               0.48 MB
    Response time:                  3.79 secs
    Transaction rate:              23.69 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                   89.84
    Successful transactions:         696
    Failed transactions:               0
    Longest transaction:           13.74
    Shortest transaction:           0.26
    200
    Transactions:                    429 hits
    Availability:                 100.00 %
    Elapsed time:                  29.11 secs
    Data transferred:              11.98 MB
    Response time:                 13.24 secs
    Transaction rate:              14.74 trans/sec
    Throughput:                     0.41 MB/sec
    Concurrency:                  195.14
    Successful transactions:         429
    Failed transactions:               0
    Longest transaction:           28.26
    Shortest transaction:           0.67
    [error] descriptor table full sock.c:402: Too many open files
    500
    Transactions:                    132 hits
    Availability:                   9.39 %
    Elapsed time:                  35.24 secs
    Data transferred:               0.18 MB
    Response time:                 17.84 secs
    Transaction rate:               3.75 trans/sec
    Throughput:                     0.01 MB/sec
    Concurrency:                   66.84
    Successful transactions:         133
    Failed transactions:            1274
    Longest transaction:           29.44
    Shortest transaction:           0.00

</details>

<details>
  <summary>innodb_flush_log_at_trx_commit=2</summary>

    10
    Transactions:                    764 hits
    Availability:                 100.00 %
    Elapsed time:                  29.56 secs
    Data transferred:               0.52 MB
    Response time:                  0.38 secs
    Transaction rate:              25.85 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                    9.95
    Successful transactions:         764
    Failed transactions:               0
    Longest transaction:            0.76
    Shortest transaction:           0.23
    50
    Transactions:                    730 hits
    Availability:                 100.00 %
    Elapsed time:                  29.70 secs
    Data transferred:               0.50 MB
    Response time:                  1.94 secs
    Transaction rate:              24.58 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                   47.72
    Successful transactions:         730
    Failed transactions:               0
    Longest transaction:            6.65
    Shortest transaction:           0.24
    100
    Transactions:                    701 hits
    Availability:                 100.00 %
    Elapsed time:                  29.58 secs
    Data transferred:               0.48 MB
    Response time:                  3.78 secs
    Transaction rate:              23.70 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                   89.55
    Successful transactions:         701
    Failed transactions:               0
    Longest transaction:           15.19
    Shortest transaction:           0.27
    200
    Transactions:                    673 hits
    Availability:                 100.00 %
    Elapsed time:                  29.45 secs
    Data transferred:               0.46 MB
    Response time:                  6.92 secs
    Transaction rate:              22.85 trans/sec
    Throughput:                     0.02 MB/sec
    Concurrency:                  158.24
    Successful transactions:         673
    Failed transactions:               0
    Longest transaction:           27.34
    Shortest transaction:           0.26
    500
    Transactions:                    245 hits
    Availability:                  16.13 %
    Elapsed time:                  35.23 secs
    Data transferred:               0.17 MB
    Response time:                  6.47 secs
    Transaction rate:               6.95 trans/sec
    Throughput:                     0.00 MB/sec
    Concurrency:                   44.97
    Successful transactions:         245
    Failed transactions:            1274
    Longest transaction:            9.08
    Shortest transaction:           0.71

</details>

### `innodb_flush_log_at_trx_commit` and concurrency comparison table (transactions/sec)
```sql
+--------------------------------+-----------+-------+---------+-------+-------+
| innodb_flush_log_at_trx_commit |     10    |   50  |    100  |  200  | 500   |
+--------------------------------+-----------+-------+---------+-------+-------+
|               0                |   26.54   | 27.00 |  25.15  | 22.62 | 7.02  |
+--------------------------------+-----------+-------+---------+-------+-------+
|               1                |   24.03   | 26.25 |  23.69  | 14.74 | 3.75  |
+--------------------------------+-----------+-------+---------+-------+-------+
|               2                |   25.85   | 24.58 |  23.70  | 22.85 | 6.95  |
+--------------------------------+-----------+-------+---------+-------+-----ß--+
```

### Additional queries to debug/observe
```sql
show full processlist;
SHOW GLOBAL VARIABLES LIKE 'innodb_flush_log%';
SHOW GLOBAL VARIABLES LIKE 'innodb_adaptive_hash_index%';
SHOW INDEX FROM users;

select TABLE_NAME as `Table`, ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024) as `Size (MB)`
from information_schema.tables
where table_schema = 'db_highload'
order by (DATA_LENGTH + INDEX_LENGTH) desc;
+-------+-----------+
| Table | Size (MB) |
+-------+-----------+
| users |         5 |
+-------+-----------+

CREATE INDEX full_name_btree_index USING BTREE ON users (full_name);
```

### References
[adaptive Hash index](https://dev.mysql.com/doc/refman/5.7/en/innodb-adaptive-hash.html)

### Questions
1. BTREE index does not improve queries where optimizer cuts out only small number of rows. (e.g. birth_date < '1950-01-10'). How to improve?
2. HASH index does not differ significantly from BTREE for range queries. Why?
3. `innodb_flush_log_at_trx_commit` seem not to have significant impact on transactions per second.
