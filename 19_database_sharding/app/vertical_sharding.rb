require 'sequel'
require 'pg'
require 'pry'


sleep 20 # Wait for PostgreSQL to load

DB = Sequel.connect('postgres://app:pass@postgresql-b:5432/mydb')

# DB.drop_table?(:books)
# DB.drop_table?(:books1)

# https://github.com/jeremyevans/sequel/blob/master/doc/schema_modification.rdoc#column-types-
DB.create_table?(:books) do
  primary_key :id
  Integer :category_id, null: false
  String :author, null: false
  String :title, null: false
  Integer :year, null: false
end

binding.pry

# books_1 shard
DB.run "CREATE TABLE books_1 (CHECK ( category_id=1 )) INHERITS ( books );"
DB.run <<-SQL
  CREATE OR REPLACE RULE books_insert_to_category_1 AS ON INSERT TO books
  WHERE ( category_id = 1 )
  DO INSTEAD INSERT INTO books_1 VALUES (NEW.*) RETURNING .*;
SQL

# books_2 shard
DB.run "CREATE TABLE books_2 (CHECK ( category_id=2 )) INHERITS ( books );"
DB.run <<-SQL
  CREATE OR REPLACE RULE books_insert_to_category_2 AS ON INSERT TO books
  WHERE ( category_id = 2 )
  DO INSTEAD INSERT INTO books_2 VALUES (NEW.*);
SQL

books = DB[:books]
books_1 = DB[:books_1]

# 10_000.times do
#   sleep 1
#   value = rand
#   books.insert(
#     category_id: 1,
#     author: 'Igor Rymaruk',
#     title: 'Try potoky misyachnogo svlitla',
#     year: 2017
#   )
# end

binding.pry
