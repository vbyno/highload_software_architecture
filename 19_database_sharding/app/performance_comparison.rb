require 'sequel'
require 'pg'
require 'pry'
require 'benchmark'


def insert_rows(table, start_id: 1, n: 1_000_000, rows_in_batch: 100)
  counter = start_id

  while counter - start_id < n do
    rows = (counter..(counter + rows_in_batch)).map do |i|
      category_id = rand(2) + 1

      [i, category_id, "Author #{category_id}", "Title-#{i}", rand(100) + 1900]
    end

    table.import([:id, :category_id, :author, :title, :year], rows)

    counter += rows_in_batch
  end
end

DB_SHARDING = Sequel.connect('postgres://app:pass@postgresql-b:5432/mydb')
DB_NO_SHARDING = Sequel.connect('postgres://app:pass@postgresql-no-sharding:5432/mydb')
# # https://github.com/jeremyevans/sequel/blob/master/doc/schema_modification.rdoc#column-types-
DB_NO_SHARDING.create_table?(:books) do
  Integer :id, null: false
  Integer :category_id, null: false
  String :author, null: false
  String :title, null: false
  Integer :year, null: false

  index :category_id
end

Benchmark.bm do |bm|
  start_id = 4000 + 1_000_000
  bm.report('Sharding   ') { insert_rows(DB_SHARDING[:books], start_id: start_id, n: 1_000_000, rows_in_batch: 1000) }
  bm.report('No Sharding') { insert_rows(DB_NO_SHARDING[:books], start_id: start_id, n: 1_000_000, rows_in_batch: 1000) }
end
