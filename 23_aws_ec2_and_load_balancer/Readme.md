# 23. AWS. EC2 and Load Balancer

## Assignment
1. Create 2 micro instances in AWS.
2. Setup classic load balancer and assign instances to it.

## 1-2 Create micro instances in AWS and assign load balancer
```bash
# if needed
# asdf plugin-add terraform https://github.com/asdf-community/asdf-hashicorp.git
cd ops
# touch main.tf outputs.tf variables.tf versions.tf

asdf install
```

Create private `terraform-highload-state` bucket in `eu-west-3 (Paris)` region.

Deploy an infrastructure with terraform

```bash
terraform init
terraform apply
```

Let's check if the load balancer works

```bash
curl "$(terraform output -raw dns_name)"

<!DOCTYPE html><h2>Hello from AWS! []</h2>
```

On the load balancer's target group there are two EC2 instances available:

<img src="img/load_balancer_target_group.png" alt="LB Target Group" width="600"/>

On the `monitoring` section, we can see the health checks are successful for both of them

<img src="img/healthy_hosts.png" alt="Healthy hosts" width="600"/>

Let's stop one instance on AWS console:

<img src="img/1_one_instance_stopped.png" alt="One EC2 instance stopeed" width="600"/>

and check if the balancer still responds:

```bash
curl "$(terraform output -raw dns_name)"

<!DOCTYPE html><h2>Hello from AWS! []</h2>
```

However, target group monitoring displays failed healthchecks for one of the instances:

<img src="img/one_unhealthy_instance.png" alt="One EC2 instance stopeed" width="600"/>

Let's stop the second instance.

<img src="img/2_two_instances_stopped.png" alt="All EC2 instances stopeed" width="600"/>

```bash
curl "$(terraform output -raw dns_name)"

<html>
<head><title>503 Service Temporarily Unavailable</title></head>
<body>
<center><h1>503 Service Temporarily Unavailable</h1></center>
</body>
</html>
```

The service is unavailable. The LB target group shows there are 2 unused instances and no healthy ones:

<img src="img/two_unused_instances.png" alt="" width="600"/>

Let's start the first instance again:

<img src="img/3_one_instance_back.png" alt="One instance back" width="600"/>

And check the LB' respond is successful:

```bash
curl "$(terraform output -raw dns_name)"

<!DOCTYPE html><h2>Hello from AWS! []</h2>
```

It is! Moreover, the target group healthchecks show there is one healthy EC2 instance:

<img src="img/one_healthy.png" alt="One instance back" width="600"/>

Now we can delete the infrastructure
```bash
terraform destroy
```
## References
- [How can I utilize user data to automatically run a script with every restart of my Amazon EC2 Linux instance?](https://aws.amazon.com/premiumsupport/knowledge-center/execute-user-data-ec2/)

## Questions
1. Why LB does not pick up EC2 instance after it was stopped? – it was not launched since `user data` was executed only once – on instance creation.
