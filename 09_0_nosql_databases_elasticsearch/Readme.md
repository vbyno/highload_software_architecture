# 09_0. NoSQL Databases. ElasticSearch

## Assignment
- Create ES index that will serve autocomplete needs with leveraging typos and errors (max 3 typos if word length is bigger than 7).
- Please use english voc. Look at google as a ref.

## Solution
Launch a Blog application:
```bash
cd shared/blog
docker-compose up
```
Populate ElasticSearch index with English words:
```bash
docker-compose exec app bundle exec rake populate_words
```

This creates a new index `words-XXXXX` and reassign existing alias `words` to it once it is created. The structure of created index:
```bash
body: {
  mappings: {
    properties: {
      word: {
        type: 'search_as_you_type',
        doc_values: false,
        max_shingle_size: 3,
        analyzer: analyzer
      }
    },
  },
  settings: {
    index: {
      number_of_shards: 1,
      number_of_replicas: 1
    },
    analysis: {
      analyzer: { default: { type: analyzer } },
      default_search: { type: analyzer }
    }
  }
}
```

Visit `http://localhost:3001/autocompletes` and type any word:
Results:

Word `Search`

<img src="img/search.png" alt="Search" width="200"/>

Word `Propagation`

<img src="img/propagation.png" alt="Propagation" width="200"/>

Under the hood, this query is applied:
```bash
"query": {
  "fuzzy": {
    "word": {
      "value": "propgateon",
      "fuzziness": 3,
      "max_expansions": 50,
      "prefix_length": 1,
      "transpositions": true
    }
  }
}
```
The fuziness is executed on the fly based on query length:
```ruby
def fuzziness
  [[(query.length - 2), 0].max / 2, 3].max
end
```
Therefore, for 8+ letters the fuziness is 3.

## References
- [English Words](https://raw.githubusercontent.com/dwyl/english-words/master/words.txt)
- [ElasticSearch Ruby Client](https://github.com/elastic/elasticsearch-ruby)
- [Elasticsearch Autocomplete with Search-As-You-Type](https://coralogix.com/blog/elasticsearch-autocomplete-with-search-as-you-type/)
- [Autosuggest](https://www.freakyjolly.com/autocomplete-suggestion-control-using-react-autosuggest/)
