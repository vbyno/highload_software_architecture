FROM kalilinux/kali-rolling:latest

RUN apt-get update -y && apt-get install -y \
  metasploit-framework \
  slowhttptest \
  hping3 \
  fping \
  siege \
  iputils-ping
