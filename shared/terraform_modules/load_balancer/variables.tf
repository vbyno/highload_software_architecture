variable "name_prefix" {
  type = string
  description = "Name"
}

variable "ec2_instance_ids" {
  type = list(string)
  description = "ids of ec2 instances to balance a load"
}

variable "subnet_ids" {
  type = list(string)
  description = "Subnets to assign"
  default = []
}

variable "autoscaling_group_ids" {
  type = list(string)
  description = "An array of autoscaling group IDs"
  default = []
}

variable "assigned_security_groups" {
  type = list(string)
  default = []
}

variable "lb_target_group_arn" {
  type = string
}
