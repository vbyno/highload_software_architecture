
resource "aws_alb" "app_lb" {
  name               = "${var.name_prefix}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.assigned_security_groups
  subnets            = var.subnet_ids

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

resource "aws_alb_listener" "app_lb_listener" {
  load_balancer_arn = aws_alb.app_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = var.lb_target_group_arn
  }
}


resource "aws_alb_target_group_attachment" "app_lb_tg_attachment" {
  count = length(var.ec2_instance_ids)
  # for_each = toset(var.ec2_instance_ids)
  # for_each = toset(["i-0b90132300f02be7b", "i-0bad5083269a877ac"])

  target_group_arn = var.lb_target_group_arn
  target_id        = element(var.ec2_instance_ids, count.index)
  # target_id        = each.key
  port             = 80
}
