variable "name_prefix" {
  type = string
  description = "Name prefix"
}

variable "aws_region" {
  type        = string
  description = "AWS Region for S3 buckets"
  default     = "eu-west-3"
}

variable "lambda_invoke_arn" {
  type        = string
  description = "Lambda ARN of Lambda current Api-Gateway triggers"
}

variable lambda_function_name {
  type        = string
  description = "Lambda Function to invoke"
}
