locals {
  sorted_subnet_ids = zipmap(
    tolist(range(var.instances_number)),
    [for i in range(var.instances_number): element(sort(var.subnet_ids), i)]
  )
}

resource "aws_instance" "ec2_instance" {
  for_each = local.sorted_subnet_ids
  subnet_id = each.value

  launch_template {
    id      = var.launch_template_id
    version = "$Latest"
  }

  lifecycle {
    ignore_changes = [tags]
  }

  tags = {
    Name = var.name_prefix
  }
}

resource "aws_eip" "default" {
  for_each = aws_instance.ec2_instance

  instance = each.value.id
  vpc      = true
}
