variable "name_prefix" {
  type = string
  description = "Name prefix of S3 bucket and all related resources"
}

variable "aws_region" {
  type        = string
  description = "AWS Region for S3 buckets"
  default     = "eu-west-3"
}
