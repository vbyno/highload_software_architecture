output "bucket_id" {
  value = aws_s3_bucket.default.id
}

output "bucket_name" {
  value = aws_s3_bucket.default.bucket
}

output "log_bucket_name" {
  value = aws_s3_bucket.log_bucket.bucket
}
