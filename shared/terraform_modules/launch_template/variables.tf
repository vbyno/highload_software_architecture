variable "name_prefix" {
  type = string
  description = "Name prefix of the EC2 instance and all related resources"
}

variable "my_public_ip" {
  type = string
  description = "Public IP address to open SSH connection from"
}

variable "ssh_local_key_path" {
  type = string
  description = "Local path to the private SSH key to connect to EC2 instance"
}

variable "assigned_security_groups" {
  type = list(string)
  description = "Security groups to assign"
  default = []
}

variable "vpc_id" {
  type = string
  description = "VPC id"
}

variable "app_version" {
  type = number
  description = "application version (to recreate ec2 instances)"
  default = 1
}

variable "instance_type" {
  type = string
  description = "AWS EC2 instance type"
  default = "t3.micro"
}
