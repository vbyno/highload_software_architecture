locals {
  image_tag  = "latest"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.name_prefix}-iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "revoke_keys_role_policy" {
  name = "${var.name_prefix}-AccessToS3"
  role = aws_iam_role.iam_for_lambda.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource aws_ecr_repository repo {
 name = "${var.name_prefix}-converter-lambda"
}

resource null_resource ecr_image {
 depends_on = [aws_ecr_repository.repo]

 triggers = {
   python_file = md5(file("${var.src}/lambda_function.py"))
   docker_file = md5(file("${var.src}/Dockerfile"))
 }

 provisioner "local-exec" {
   command = <<EOF
           aws ecr get-login-password --region ${var.aws_region} | docker login --username AWS --password-stdin ${aws_ecr_repository.repo.registry_id}.dkr.ecr.${var.aws_region}.amazonaws.com
           cd ${var.src}
           sh tag_and_push_image.sh ${aws_ecr_repository.repo.repository_url}
       EOF
 }
}

data aws_ecr_image lambda_image {
 depends_on = [null_resource.ecr_image]

 repository_name = aws_ecr_repository.repo.name
 image_tag       = local.image_tag
}

resource "aws_lambda_function" "default" {
  depends_on = [null_resource.ecr_image]

  function_name = "${var.name_prefix}-converter"
  role          = aws_iam_role.iam_for_lambda.arn
  timeout       = 300
  description   = var.description
  image_uri     = "${aws_ecr_repository.repo.repository_url}@${data.aws_ecr_image.lambda_image.id}"
  package_type  = "Image"

  reserved_concurrent_executions = 1

  environment {
    variables = {
      environment = "production"
      bucket = var.s3_bucket_name
    }
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.default.arn
  principal     = "s3.amazonaws.com"
  source_arn    = var.s3_bucket_arn
}

resource "aws_lambda_function_event_invoke_config" "default" {
  function_name = aws_lambda_function.default.function_name
  depends_on = [aws_iam_role_policy.revoke_keys_role_policy]
}

resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${aws_lambda_function.default.function_name}"
  retention_in_days = 1
}

# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "lambda_logging" {
  name        = "${var.name_prefix}_lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}
