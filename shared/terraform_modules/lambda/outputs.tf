output "lambda_arn" {
  value = aws_lambda_function.default.arn
}

output "invoke_arn" {
  value = aws_lambda_function.default.invoke_arn
}

output "function_name" {
  value = aws_lambda_function.default.function_name
}
