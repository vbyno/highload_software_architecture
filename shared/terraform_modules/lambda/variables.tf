variable "name_prefix" {
  type = string
  description = "Name prefix of the infrastructure elements"
}

variable "aws_region" {
  type        = string
  description = "AWS Region for lambdas"
  default     = "eu-west-3"
}

variable "src" {
  type        = string
  description = "An absolute path to lambda directory containing lambda_function.py"
}

variable "description" {
  type        = string
  description = "Lambda description"
  default = "Lambda description"
}

variable "s3_bucket_arn" {
  type        = string
  description = "S3 bucket to put files"
}

variable "s3_bucket_name" {
  type        = string
  description = "The name of S3 bucket to put files"
}
