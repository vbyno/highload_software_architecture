output "lb_tagret_group_arn" {
  value = aws_alb_target_group.app_lb_target_group.arn
}

output "lb_tagret_group_arn_suffix" {
  value = aws_alb_target_group.app_lb_target_group.arn_suffix
}
