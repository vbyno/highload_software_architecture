resource "aws_alb_target_group" "app_lb_target_group" {
  name        = "${var.name_prefix}-target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id

  health_check {
    enabled = true
    path = "/"
    interval = 5
    timeout = 2
  }
}
