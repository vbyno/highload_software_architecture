locals {
  net_threshold = 100
  net_threshold_down = 400
  cpu_policy_count = var.scaling_policy == "cpu" ? 1 : 0
  requests_policy_count = var.scaling_policy == "requests" ? 1 : 0
}
resource "aws_autoscaling_group" "app_autoscaling_group" {
  name = "${var.name_prefix} - group"
  capacity_rebalance  = true
  desired_capacity    = var.desired_capacity
  max_size            = var.max_size
  min_size            = var.min_size
  vpc_zone_identifier = var.subnet_ids
  health_check_grace_period = 120
  health_check_type         = "ELB"
  target_group_arns         = [var.lb_target_group_arn]
  wait_for_capacity_timeout = 0 # Do not wait for new capasity

  mixed_instances_policy {
    instances_distribution {
      on_demand_allocation_strategy            = "prioritized"
      on_demand_base_capacity                  = 1
      on_demand_percentage_above_base_capacity = 0
      spot_allocation_strategy                 = "capacity-optimized-prioritized"
      spot_instance_pools                      = 0
    }

    launch_template {
      launch_template_specification {
        launch_template_id   = var.launch_template_id
        version              = "$Latest"
      }

      override {
        instance_type     = "t3.micro"
        weighted_capacity = "2"
      }
      override {
        instance_type     = "t2.micro"
        weighted_capacity = "2"
      }
      override {
        instance_type     = "t3.small"
        weighted_capacity = "2"
      }
      override {
        instance_type     = "t2.small"
        weighted_capacity = "2"
      }
    }
  }

  enabled_metrics = [
    "GroupAndWarmPoolDesiredCapacity",
    "GroupAndWarmPoolTotalCapacity",
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingCapacity",
    "GroupPendingInstances",
    "GroupStandbyCapacity",
    "GroupStandbyInstances",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
    "WarmPoolDesiredCapacity",
    "WarmPoolMinSize",
    "WarmPoolPendingCapacity",
    "WarmPoolTerminatingCapacity",
    "WarmPoolTotalCapacity",
    "WarmPoolWarmedCapacity",
  ]

  lifecycle {
    create_before_destroy = true

    ignore_changes = [
      target_group_arns
    ]
  }
}

resource "aws_autoscaling_policy" "cpu_policy" {
  count                     = local.cpu_policy_count

  autoscaling_group_name    = aws_autoscaling_group.app_autoscaling_group.name
  name                      = "${var.name_prefix} - Average CPU Scaling Policy"
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 10

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40.0
  }
}

resource "aws_autoscaling_policy" "network_policy" {
  count                    = local.requests_policy_count

  name                     = "${var.name_prefix} - HTTP Requests Policy"
  policy_type              = "StepScaling"
  adjustment_type          = "PercentChangeInCapacity"
  min_adjustment_magnitude = 1
  autoscaling_group_name   = aws_autoscaling_group.app_autoscaling_group.name

  step_adjustment {
    scaling_adjustment          = 50
    metric_interval_lower_bound = local.net_threshold
    metric_interval_upper_bound = local.net_threshold * 2
  }

  step_adjustment {
    scaling_adjustment          = 50
    metric_interval_lower_bound = local.net_threshold * 2
  }
}

resource "aws_cloudwatch_metric_alarm" "request_count_alarm" {
  count               = local.requests_policy_count

  alarm_name          = "${var.name_prefix} - Network Alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "RequestCountPerTarget"
  namespace           = "AWS/ApplicationELB"
  statistic           = "Sum"
  evaluation_periods  = 1
  period              = 30
  threshold           = local.net_threshold
  alarm_description   = "This metric monitors HTTP requests number per target group"
  alarm_actions       = [element(aws_autoscaling_policy.network_policy, count.index).arn]

  dimensions = {
    TargetGroup = var.lb_target_group_arn_suffix
  }
}
