variable "name_prefix" {
  type = string
  description = "Name prefix"
}

variable "subnet_ids" {
  type = list(string)
  description = "Vpc subnets"
}

variable "launch_template_id" {
  type = string
  description = "Template to launch every instance"
}

variable "desired_capacity" {
  type = number
}

variable "min_size" {
  type = number
}

variable "max_size" {
  type = number
  default = 6
}

variable "lb_target_group_arn" {
  type = string
}

variable "lb_target_group_arn_suffix" {
  type = string
}

variable "scaling_policy" {
  type = string
  default = "cpu"
  description = "Policy to scale. Values: `cpu`, `requests`"
}
