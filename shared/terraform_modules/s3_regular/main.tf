resource "aws_s3_bucket" "default" {
  bucket = "${var.name_prefix}-regular-bucket"
  acl    = "private"
  force_destroy = true # destroy all objects in the bucket when it is destroyed

  versioning {
    enabled = false
  }
}

resource "aws_s3_bucket_public_access_block" "access_block" {
  bucket = aws_s3_bucket.default.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
