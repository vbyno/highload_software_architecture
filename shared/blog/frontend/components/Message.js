import React from "react"

function Message(props) {
  return <h3>{props.text}</h3>
};

export default Message
