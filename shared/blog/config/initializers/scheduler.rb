require 'rufus-scheduler'

Rufus::Scheduler.singleton.tap do |s|
  next unless ENV['TRACK_CURRENCY_RATE_PERIOD']

  s.every ENV['TRACK_CURRENCY_RATE_PERIOD'] do
    PushCurrencyRatioToGoogleAnalytics.call
    Rails.logger.info "Currency Ratio Tracked at #{Time.now}"
  end
end
