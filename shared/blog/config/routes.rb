Rails.application.routes.draw do
  resources :posts
  resources :users, only: :index
  resources :autocompletes, only: :index

  # /images/photo1.jpeg request bypasses the controller
  # because of the Nginx config:
  # try_files $uri $uri/index.html $uri.html @app;
  # I'm changing the route name to test the controller properly
  get '/getimages/:filename', to: 'images#show'

  root to: 'posts#index'
end
