#! /bin/sh

# bundle exec rails webpacker:install webpacker:install:react
# bundle exec rails generate react:install
# or
# RAILS_ENV=production RACK_ENV=production NODE_ENV=production bundle exec rails assets:precompile webpacker:compile

rm -f tmp/pids/server.pid
export NODE_OPTIONS="--max_old_space_size=4096" && yarn install

bundle exec foreman run rails db:create db:migrate db:seed
bundle exec foreman start -f Procfile
