#! /bin/sh

# bundle exec rails webpacker:install
# or
# RAILS_ENV=production RACK_ENV=production NODE_ENV=production bundle exec rails assets:precompile webpacker:compile

rm -f tmp/pids/server.pid

bundle exec foreman run rails db:create db:migrate db:seed
bundle exec foreman start -f Procfile.production
