class UsersController < ApplicationController
  helper_method :created_users

  def index
    @created_users =
      ENV
        .fetch('NUMBER_OF_USERS_TO_CREATE', 10)
        .to_i
        .times
        .map do
          i = rand(100000)

          User.create!(
            full_name: "Volodymyr-#{i}",
            birth_date: i.days.ago
          )
        end
  end

  private

  attr_reader :created_users
end
