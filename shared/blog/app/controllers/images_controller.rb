class ImagesController < ApplicationController
  def show
    # This is an essential line, which defines Cache-Control header
    # https://leshchuk.medium.com/http-cache-on-rails-nginx-stack-950fee2f8eef
    expires_in 1.day, public: true
    send_file Rails.root.join('public', 'images', "#{params[:filename]}.jpeg"),
              type: "image/jpeg",
              disposition: "inline",
              x_sendfile: true
  end
end
