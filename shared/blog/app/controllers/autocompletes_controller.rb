class AutocompletesController < ApplicationController
  helper_method :autocompletes

  def index; end

  private

  def autocompletes
    ElasticSearch::SearchWords.call(params['s'])
  end
end
