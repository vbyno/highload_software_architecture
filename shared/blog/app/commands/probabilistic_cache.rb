class ProbabilisticCache
  def self.call(...)
    new(...).call
  end

  attr_reader :key, :expires_in, :probability_interval, :block

  def initialize(key:, expires_in:, probability_interval:, &block)
    @key = key
    @expires_in = expires_in
    @probability_interval = probability_interval
    @block = block
  end

  def call
    return block.call unless cache_flushing?

    ttl = cache.redis.ttl(key)

    if ttl <= 0 || rand < probability(ttl)
      logger.info('*** Data is being written to cache') if debug_mode?
      calculate_result_and_write_cache
    else
      logger.info('*** Cache data is being read') if debug_mode?
      cache.read(key) || calculate_result_and_write_cache
    end
  end

  private

  delegate :cache, :logger, to: :Rails

  def calculate_result_and_write_cache
    block.call.tap { |result| cache.write(key, result, expires_in: expires_in) }
  end

  def probability(ttl)
    [probability_interval - ttl, 0].max.fdiv(probability_interval).tap do |p|
      logger.info "*** TTL: #{ttl}; Probability: #{p}" if debug_mode?
    end
  end

  def debug_mode?
    ENV['PROBABILISTIC_CACHE_FLUSHING_DEBUG'] == 'true'
  end

  def cache_flushing?
    ENV['PROBABILISTIC_CACHE_FLUSHING'] == 'true'
  end
end
