# frozen_string_literal: true

class PushCurrencyRatioToGoogleAnalytics
  USD_CURRENCY_CODE = 'USD'

  def self.call(*args)
    new(*args).call
  end

  def call
    ga_track_event('Banking', 'Currency Rate', 'uah/usd', (usd_currency_rate * 100).to_i)
  end

  private

  def ga_track_event(category, action, label, value)
    # Anonymous Client ID.
    # Ideally, this should be a UUID that is associated
    # with particular user, device, or browser instance.
    client_id = "555"

    Net::HTTP.post_form(
      # URI('https://www.google-analytics.com/debug/collect'),
      URI('https://www.google-analytics.com/collect'),
      v:   '1',                 # API Version
      tid: google_analytics_id, # Tracking ID / Property ID
      cid: client_id,           # Client ID
      t:   'event',             # Event hit type
      ec:  category,            # Event category
      ea:  action,              # Event action
      el:  label,               # Event label
      ev:  value                # Event value
    )
  end

  def usd_currency_rate
    bank_currencies
      .detect { |c| c.fetch(:cc) == USD_CURRENCY_CODE }
      .fetch(:rate)
  end

  def bank_currencies
    JSON.parse(Net::HTTP.get(URI(bank_currency_url)), symbolize_names: true)
  end

  def google_analytics_id
    ENV.fetch('GOOGLE_ANALYTICS_TRACKING_ID')
  end

  def bank_currency_url
    ENV.fetch('BANK_CURRENCY_URL')
  end
end
