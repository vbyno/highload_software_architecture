class MysqlApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  connects_to database: { writing: :primary_mysql, reading: :primary_mysql }
end
