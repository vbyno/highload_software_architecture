# frozen_string_literal: true

module ElasticSearch
  class PopulateWords
    def self.call(...)
      new(...).call
    end

    attr_reader :client, :index_alias, :index_name, :filename

    def initialize(index_alias: 'words', filename: File.expand_path('words.txt', __dir__))
      # client.transport.reload_connections!
      @client = Elasticsearch::Client.new(url: ENV['ELASTICSEARCH_URL'], log: true)
      @client.cluster.health
      @index_alias = index_alias
      @index_name = "#{index_alias}-#{current_timestamp}"
      @filename = filename
      # client.search(q: 'test')
    end

    def call
      create_new_index
      populate_new_index
      switch_alias_to_new_index
      delete_old_indices
    end

    private

    def create_new_index(analyzer: 'standard')
      client.indices.create(
        index: index_name,
        id: 3,
        body: {
          mappings: {
            properties: {
              word: {
                type: 'search_as_you_type',
                doc_values: false,
                max_shingle_size: 3,
                analyzer: analyzer
              }
            },
          },
          settings: {
            index: {
              number_of_shards: 1,
              number_of_replicas: 1
            },
            analysis: {
              analyzer: { default: { type: analyzer } },
              default_search: { type: analyzer }
            }
          }
        }
      )
    end

    def populate_new_index
      File.foreach(filename).each_slice(1000) do |lines|
        body = lines.map { |line| { index: { data: { word: line.strip } } } }

        client.bulk(index: index_name, body: body )
      end
    end

    def switch_alias_to_new_index
      client.indices.update_aliases(body: {
        actions: [{ add: { index: index_name, alias: index_alias } }]
      })
    end

    def delete_old_indices
      client
        .cat
        .indices(format: :json)
        .map { |hash| hash.fetch('index') }
        .select { |old_index_name| old_index_name.start_with?(index_alias) }
        .each do |old_index_name|
          client.indices.delete(index: old_index_name) if old_index_name != index_name
        end
    end

    def current_timestamp
      Time.current.strftime('%Y%m%d%H%M%S')
    end
  end
end
