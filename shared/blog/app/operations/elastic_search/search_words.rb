# frozen_string_literal: true

module ElasticSearch
  class SearchWords
    INDEX = 'words'

    def self.call(...)
      new(...).call
    end

    attr_reader :query

    def initialize(query)
      @query = query
    end

    # https://github.com/elastic/elasticsearch-ruby/blob/main/elasticsearch-api/README.md#usage-with-the-elasticsearch-gem
    def call
      client
        .search(index: INDEX, body: query_body)
        .dig('hits', 'hits')
        .then { |hits| hits || [] }
        .map { |hit| hit.fetch('_source') }
        .map { |hit| OpenStruct.new(hit) }
    end

    def client
      @client ||= Elasticsearch::Client.new(url: ENV['ELASTICSEARCH_URL'], log: true)
    end

    # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-fuzzy-query.html
    def query_body
      # { query: { multi_match: { query: query, fields: ['word'] } } }
      {
        query: {
          fuzzy: {
            word: {
              value: query,
              fuzziness: fuzziness,
              prefix_length: 1
            }
          }
        }
      }
    end

    # allow 3 typos if length > 7
    def fuzziness
      [[(query.length - 2), 0].max / 2, 3].max
    end
  end
end
