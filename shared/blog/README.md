# README
## Rails Application
[Get Start With Mongo](https://docs.mongodb.com/mongoid/current/tutorials/getting-started-rails/)
```
# No ASDF, since mysql is not installed
sudo gem install rails -v '~> 6.1.4'
rails new blog --skip-active-record --skip-bundle
npm install --global yarn
rails webpacker:install
```

To install ruby and nodejs
```bash
asdf install
rails g react:component HelloWorld greeting:string
rm -rf node_modules && yarn
brew install mysql-connector-c
```

### Run application
```bash
foreman run bin/webpack-dev-server
foreman run bundle exec rails s
docker-compose up mysql redis mongo elasticsearch elasticsearch-hq
```

```
bin/rails assets:clobber
$/myapp> bin/rails webpacker:compile
```

## References
- [React-rails](https://github.com/reactjs/react-rails)
- [MySQL with homebriew](https://coderwall.com/p/whbzrw/install-mysql2-gem-in-mac-os-x-with-mysql-installed-via-brew)
- [Using Bootstrap with Rails Webpacker](https://rossta.net/blog/webpacker-with-bootstrap.html)
- [Bootstrap Rails](https://bootrails.com/blog/rails-bootstrap-tutorial)
