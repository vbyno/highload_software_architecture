# 13. Logging

## Homework
1) Set up MySQL with slow query log
2) Configure ELK to work with mysql slow query log
3) Configure GrayLog2 to work with mysql slow query log

## 1. Set up MySQL with slow query log

```bash
docker-compose -f docker-compose.mysql.yml up
docker-compose -f docker-compose.mysql.yml  exec mysql mysql -u app -D db_highload -p

> SELECT SLEEP(5);
```

since we've configured in `mysql.cnf` to log every query longer than 3 seconds, now we can see it being reported in `slow.log`:

```bash
# Time: 2022-01-29T06:26:17.493702Z
# User@Host: app[app] @ localhost []  Id:     8
# Query_time: 5.000731  Lock_time: 0.000000 Rows_sent: 1  Rows_examined: 1
SET timestamp=1643437572;
select sleep(5);
```

## 2. Configure ELK to work with mysql slow query log
```bash
docker-compose -f docker-compose.elk.yml up
```

Run slow query in MySQL:
```bash
select sleep(6);
```

Go to [http://localhost:5601/discovery](http://localhost:5601/discovery) and add `filebeat` index to Kibana

As a result, the query is displayed on dashboard.

<img src="img/kibana_mysql_slow_logs.png" alt="Kibana MySQL Slow Query Log" width="500"/>

Moreover, the logs contain the actual query that took 6 seconds to execute.

```json
...
"mysql": {
    "thread_id": 8,
    "slowlog": {
      "lock_time": {
        "sec": 0
      },
      "rows_sent": 1,
      "rows_examined": 1,
      "query": "select sleep(6);",
      "current_user": "app"
    }
  },
...
```

Turn down ELK containers
```bash
docker-compose -f docker-compose.mysql.yml down
```

## 3. Configure GrayLog2 to work with mysql slow query log
```bash
docker-compose -f docker-compose.graylog.yml up
```
1. Visit [http://localhost:9000](http://localhost:9000) with creds `admin/password`.
Go to `System -> Content Packs` and upload [mysql_slow_query_content_pack.json](ops/graylog/mysql_slow_query_content_pack.json) from `ops/graylog` folder.

2. Install MySQL Slow Query Log GROK Pattern

<img src="img/graylog_install_pattern.png" alt="Install Slow Query Log" width="500"/>

3. Go to `System -> Inputs` and add a new input named `mysql-slow`.

4. For this input `Manage Extractors -> Add Extractor`.

<img src="img/graylog_add_extractor.png" alt="Add Extractor" width="500"/>

5. Edit bin interval and observe the dashboard

<img src="img/graylog_dashboard.png" alt="Graylog Dashboard" width="500"/>

### References
- [The Elastic stack (ELK) powered by Docker and Compose.](https://github.com/deviantony/docker-elk)
- [Filebeat overview](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-overview.html)
- [tugrulcan/docker-elk-filebeat](https://github.com/tugrulcan/docker-elk-filebeat)
- [Docker Logs with the ELK Stack—Part I](https://logz.io/blog/docker-logging/)
- [Filebeat MySQL module](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-module-mysql.html)
- [Graylog2 Compose](https://github.com/pecigonzalo/graylog-compose)
- [MySQL Slow Query LOG GROK pattern for Graylog](https://github.com/zionio/graylog_grok_mysqlslowquery)
### Questions
1. Do we need logstash if we have filebeat?
2. How to save the configuration for graylog?
