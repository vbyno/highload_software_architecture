# 07. Web Servers

## Assignment
1. Configure nginx that will cache only images, that were requested at least twice
2. Add ability to drop nginx cache by request. You should drop cache for specific file only (not all cache)

## 1. Configure nginx that will cache only images, that were requested at least twice
The `nginx` config is located in [shared/blog/ops/docker/nginx.conf](shared/blog/ops/docker/nginx.conf).
To start the server with Nginx:
```bash
cd ../shared/blog
docker-compose up
```

Get the first picture:
```bash
curl http://localhost/getimages/1.jpeg -I

HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:13:56 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="1.jpeg"; filename*=UTF-8''1.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: fab49007-81f2-40ac-8dd7-cd741e75ec63
X-Runtime: 0.020081
Cache-Control: public
X-Cache-Status: MISS
```

It is not cached. Let's get it for the second time:
```bash
curl http://localhost/getimages/1.jpeg -I

HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:14:40 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="1.jpeg"; filename*=UTF-8''1.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: 2ee70129-361f-4ad5-8549-fba2c6d38f75
X-Runtime: 0.014327
Cache-Control: public
X-Cache-Status: MISS
```

It is not cached. Let's get it for the third time:
```bash
curl http://localhost/getimages/1.jpeg -I

HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:15:40 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="1.jpeg"; filename*=UTF-8''1.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: 2ee70129-361f-4ad5-8549-fba2c6d38f75
X-Runtime: 0.014327
Cache-Control: public
X-Cache-Status: HIT
```

It is cached now, because `X-Cache-Status: HIT`.

Let's check if photo 2 is not cached:
```bash
curl http://localhost/getimages/2.jpeg -I

HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:16:27 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="2.jpeg"; filename*=UTF-8''2.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: a2901076-c0e3-49d5-863a-db75235d14d6
X-Runtime: 0.010740
Cache-Control: public
X-Cache-Status: MISS
```

It is not.
## 2. Add ability to drop nginx cache by request. You should drop cache for specific file only (not all cache)

Before dropping the cache, let's cache the image #2 and check how the cache is displayed in the browser:

<img src="img/before_drop_1.png" alt="First image before cache drop" width="200"/>
<img src="img/before_drop_2.png" alt="Second image before cache drop" width="200"/>

Now let's replace the first image with the newer (`1_1.jpeg`) version:
```bash
mv public/images/1.jpeg public/images/1_0.jpeg
mv public/images/1_1.jpeg public/images/1.jpeg
```
Let's check the browser still returns the cached version of the first image:

<img src="img/before_drop_1_after_rename.png" alt="First image cache after it was replaced" width="200"/>

Accorgding to our configuration ([shared/blog/ops/docker/nginx.conf](shared/blog/ops/docker/nginx.conf)), we can invalidate the cache with the request from the localhost. Therefore, we need to execute the command:
```bash
cd shared/blog
docker-compose exec nginx curl http://localhost/getimages/1.jpeg -I -H "secret-header: true"

HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:24:10 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="1.jpeg"; filename*=UTF-8''1.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: ed3aea7e-7355-416f-92df-d2d80c8bd140
X-Runtime: 0.011052
Cache-Control: public
X-Cache-Status: BYPASS
```
As we can see, the request is handled by application server. The new requests return `X-Cache-Status: HIT`, hence the picture is cached again. The request for `2.jpeg` still returns `X-Cache-Status: HIT`, so its cache was not invalidated.

```bash
curl http://localhost/getimages/1.jpeg -I
HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:24:46 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="1.jpeg"; filename*=UTF-8''1.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: ed3aea7e-7355-416f-92df-d2d80c8bd140
X-Runtime: 0.011052
Cache-Control: public
X-Cache-Status: HIT

curl http://localhost/getimages/2.jpeg -I
HTTP/1.1 200 OK
Server: nginx
Date: Sun, 02 Jan 2022 21:24:51 GMT
Content-Type: image/jpeg
Connection: keep-alive
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Disposition: inline; filename="2.jpeg"; filename*=UTF-8''2.jpeg
Content-Transfer-Encoding: binary
Cache-Control: max-age=86400, public
X-Request-Id: c6d5047d-aed4-45df-9366-d9a2a97cdd9d
X-Runtime: 0.009484
Cache-Control: public
X-Cache-Status: HIT
```

Let's check the new version of image is displayed in the browser:

<img src="img/after_drop_1.png" alt="The updated cache of the first image" width="200"/>

`1.1` is displayed there, which is the newer version.

### Useful Commands
In Nginx container:
```bash
cat /proc/sys/net/core/somaxconn
4096
```

## References
- [A Guide to Caching with NGINX and NGINX Plus](https://www.nginx.com/blog/nginx-caching-guide/)
- [How to purge Nginx Proxy Cache in Linux CentOS](https://www.ryadel.com/en/nginx-purge-proxy-cache-delete-invalidate-linux-centos-7/)
- [HTTP cache on Rails + Nginx stack](https://leshchuk.medium.com/http-cache-on-rails-nginx-stack-950fee2f8eef)
- [Convert Text to Image](https://text2image.com/en/)

## Questions
1. Is it possible to mount cache in memory with tmpfs and save it on disk upon grasefull shut down so it is saved after container reboot?
