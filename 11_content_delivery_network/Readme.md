# 11. Content Delivery Network

## Assignment
1. Create your own CDN for delivering millions of images across the globe.
Set up 5 containers - bind server, load balancer, node1, node2, node3.
2. Try to implement different balancing approaches.
3. Implement efficient caching.

## 1. Create own CDN
Download `GeoIP.acl` file containing IP addresses and countries from [here](https://geoip.site/#script). Note: I've tried to download up-to-date ZIP-file from [MaxMind](https://www.maxmind.com/en/accounts/650389/geoip/downloads) but the structure of archive is away from  what GeoIP.sh is expecting.

Start docker-compose
```bash
docker-compose up
```
In [named.conf](ops/bind/named.conf) we have this configuration:
```bash
acl "mylocalua" {
  172.16.238.50;
};

view "ukraine" {
  match-clients { mylocalua; UA; };
  recursion no;

  zone "cdn.example.com" {
    type master;
    file "/etc/bind/geo/cdn-ua.example.com";
  };
};
```

Therefore, we expect the output of curl from `curl_ua` container (which ip is `172.16.238.50`) to be different from `curl_usa` container's output. Let's validate this:

```bash
docker-compose exec curl_ua curl cdn.example.com/public/img.txt
1111111111111111111111111111111111111111
1111111111111111111111111111111111111111
1111111111111111111111111111111111111111
1111111111111111111111111111111111111111
1111111111111111111100000111111111111111
1111111111111111111000000111111111111111
1111111111111111100000000111111111111111
1111111111111110000000000111111111111111
1111111111111100000000000111111111111111
1111111111110000000000000111111111111111
1111111111100000100000000111111111111111
1111111111100111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111111100000000111111111111111
1111111111111110000000000000111111111111
1111111111100000000000000000000111111111
1111111111100000000000000000000111111111

docker-compose exec curl_usa curl cdn.example.com/public/img.txt
1111111111111111111111111111111111111111
1111111111111111111111111111111111111111
1111111111111111111111111111111111111111
1111111111110000000000000111111111111111
1111111111000000000000000001111111111111
1111111111000000011000000000111111111111
1111111111000011111110000000011111111111
1111111111000011111111000000001111111111
1111111111000111111111100000000111111111
1111111111000111111111100000000111111111
1111111111000111111111100000000111111111
1111111111000111111111100000000111111111
1111111111111111111111100000000111111111
1111111111111111111111100000000111111111
1111111111111111111111100000000111111111
1111111111111111111111100000000111111111
1111111111111111111111100000000111111111
1111111111111111111111100000001111111111
1111111111111111111111000000001111111111
1111111111111111111111000000011111111111
1111111111111111111110000000111111111111
1111111111111111111110000001111111111111
1111111111111111111100000011111111111111
1111111111111111111000000111111111111111
1111111111111111110000001111111111111111
1111111111111111100000011111111111111111
1111111111111111000000111111111111111111
1111111111111110000001111111111111111111
1111111111111100000011111111111111111111
1111111111111000001111111111111111111111
1111111111111000011111111111111111111111
1111111111110000111111111111111111111111
1111111111100001111111111111111111111111
1111111111000000000000000000000011111111
1111111110000000000000000000000011111111
1111111110000000000000000000000011111111
1111111110000000000000000000000011111111
1111111110000000000000000000000011111111
1111111110000000000000000000000011111111
1111111111111111111111111111111111111111
```
As we can see, the same URL address returns different files, because DNS resolved IP address of `curl_ua` container as ones which maches `ukraine` view.

### [DOES NOT WORK] Attempt to validate the concept with VPN and ngrok
Expose local port via ngrok:
```bash
ngrok http 8080
```

Visit `http://22ea-176-120-105-105.ngrok.io/public/img.png` – it displays image "2", which means the client IP was not matched as UA, even though the log states the opposite:
```
# nginx_app access.log
156.146.50.110 - - [18/Dec/2021:10:02:49 +0000] "GET /public/img.png HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36"
156.146.50.110 - - [18/Dec/2021:10:02:50 +0000] "GET /favicon.ico HTTP/1.1" 500 572 "http://22ea-176-120-105-105.ngrok.io/public/img.png" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36"
```

## 2. Implement different balancing approaches
### Round-robin
Requests to the application servers are distributed in a round-robin fashion.
Configuration:
```bash
// nginx_load_balancer_ua.conf
upstream node {
  server node-ua1:80;
  server node-ua2:80;
  server node-ua3:80;
}
```
Results:
```
siege "http://localhost:7010/public/img.png" -c200 -t30s

Transactions:                  10873 hits
Availability:                 100.00 %
Elapsed time:                  29.50 secs
Data transferred:             121.33 MB
Response time:                  0.54 secs
Transaction rate:             368.58 trans/sec
Throughput:                     4.11 MB/sec
Concurrency:                  197.65
Successful transactions:       10873
Failed transactions:               0
Longest transaction:            1.44
Shortest transaction:           0.02
```

### Least connected
The next request is assigned to the server with the least number of active connections.
Configuration:
```bash
// nginx_load_balancer_ua.conf
upstream node {
  least_conn;
  server node-ua1:80;
  server node-ua2:80;
  server node-ua3:80;
}
```
Results:
```
siege "http://localhost:7010/public/img.png" -c200 -t30s

Transactions:                  11243 hits
Availability:                 100.00 %
Elapsed time:                  29.84 secs
Data transferred:             125.46 MB
Response time:                  0.53 secs
Transaction rate:             376.78 trans/sec
Throughput:                     4.20 MB/sec
Concurrency:                  197.90
Successful transactions:       11243
Failed transactions:               0
Longest transaction:            1.35
Shortest transaction:           0.01
```

### Session persistence
A hash-function is used to determine what server should be selected for the next request (based on the client’s IP address). Configuration:
```bash
// nginx_load_balancer_ua.conf
upstream node {
  ip_hash;
  server node-ua1:80;
  server node-ua2:80;
  server node-ua3:80;
}
```
Results:
```
siege "http://localhost:7010/public/img.png" -c200 -t30s

Transactions:                   9162 hits
Availability:                 100.00 %
Elapsed time:                  29.39 secs
Data transferred:             102.24 MB
Response time:                  0.63 secs
Transaction rate:             311.74 trans/sec
Throughput:                     3.48 MB/sec
Concurrency:                  196.73
Successful transactions:        9162
Failed transactions:               0
Longest transaction:            1.69
Shortest transaction:           0.02
```

### Results
```bash
+---------------------+--------------+
| Isolation           | Transaction  |
|   level             | rate         |
+---------------------+--------------+
| Round Robin         |   368.58     |
+---------------------+--------------+
| Least connected     |   376.78     |
+---------------------+--------------+
| Session persistence |   311.74     |
+---------------------+--------------+
```

## 3. Implement efficient caching
Update the configuration by using `nginx_balancer_ua_with_cache.conf` file.
```
Results:
```bash
Transactions:                  16434 hits
Availability:                 100.00 %
Elapsed time:                  29.24 secs
Data transferred:             183.39 MB
Response time:                  0.23 secs
Transaction rate:             562.04 trans/sec
Throughput:                     6.27 MB/sec
Concurrency:                  129.92
Successful transactions:       16434
Failed transactions:               0
Longest transaction:            7.01
Shortest transaction:           0.02
```
Transaction rate increased to `562.04` trans/sec.

## Useful Commands
To check the hops times:
```bash
traceroute -I google.com
```
## References
- [CDN на коленке: Часть 1. Geo DNS](https://rascal.su/blog/2012/12/15/cdn-%D0%BD%D0%B0-%D0%BA%D0%BE%D0%BB%D0%B5%D0%BD%D0%BA%D0%B5-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-geo-dns/)
- [Mirror to download CSV file with geodata](https://files-cdn.liferay.com/mirrors/geolite.maxmind.com/download/geoip/database/)
- [How to Read a Traceroute](https://www.inmotionhosting.com/support/server/ssh/read-traceroute/)
- [Docker-bind DNS](https://github.com/sameersbn/docker-bind)
- [Submarine Traffic Map](https://www.submarinecablemap.com/)
- [Nginx and BIND as a DNS server](https://www.nginx.com/blog/dns-service-discovery-nginx-plus/)
- [Download Geo-File](https://dev.maxmind.com/geoip/geolite2-free-geolocation-data?lang=en)
- [Nginx Load Balancing](https://nginx.org/en/docs/http/load_balancing.html)
- [How do I forward client IP instead of proxy IP in Nginx Reverse Proxy?](https://www.digitalocean.com/community/questions/how-do-i-forward-client-ip-instead-of-proxy-ip-in-nginx-reverse-proxy)
- [Как в Docker + Nginx + PHP-Fpm получить реальный IP пользователя?](https://qna.habr.com/q/570728)
- [Module ngx_http_realip_module](https://nginx.org/en/docs/http/ngx_http_realip_module.html)
- [Binary image](https://www.dcode.fr/binary-image)

## Questinons
- How to properly test DNS???
- Is Load Balancer a proper place to cache?
