# 04. Stress Testing. Approaches and tools

## Assignment
### Stress Test Practice
  - Create simple web page that accepts requests and stores data from request to database
  - Prepare siege urls file
  - Run siege with different concurrency ( 10, 25, 50, 100 )
  - Find resource availability, avg response time, throughput
  - Implement probabilistic cache flushing

## Load Testing
1. Install siege
```bash
brew install siege
# And create a config file
siege -C
```

2. Run the app
```bash
cd ../shared/blog && docker-compose up
# In case you need to debug in the console:
docker container exec -it `docker container ls | grep blog_app | awk '{print $1;}'` bundle exec rails c
```

3. Stress Testing
```bash
siege -f urls.txt -R siege.conf -t60s -i -v -c 100
```
BTW, there is an option `--json-output`

10 threads
```bash
Transactions:                   4177 hits
Availability:                 100.00 %
Elapsed time:                  59.46 secs
Data transferred:              84.66 MB
Response time:                  0.14 secs
Transaction rate:              70.25 trans/sec
Throughput:                     1.42 MB/sec
Concurrency:                    9.97
Successful transactions:        4175
Failed transactions:               0
Longest transaction:            2.73
Shortest transaction:           0.00
```

25 threads
```bash
Transactions:                   4449 hits
Availability:                 100.00 %
Elapsed time:                  59.65 secs
Data transferred:              89.83 MB
Response time:                  0.33 secs
Transaction rate:              74.59 trans/sec
Throughput:                     1.51 MB/sec
Concurrency:                   24.79
Successful transactions:        4449
Failed transactions:               0
Longest transaction:            3.52
Shortest transaction:           0.00
```

50 Threads
```bash
Transactions:                   4389 hits
Availability:                 100.00 %
Elapsed time:                  59.60 secs
Data transferred:              89.29 MB
Response time:                  0.66 secs
Transaction rate:              73.64 trans/sec
Throughput:                     1.50 MB/sec
Concurrency:                   48.75
Successful transactions:        4389
Failed transactions:               0
Longest transaction:            8.73
Shortest transaction:           0.00
```

100 threads
```bash
Transactions:                   5148 hits
Availability:                 100.00 %
Elapsed time:                  59.11 secs
Data transferred:             104.60 MB
Response time:                  1.09 secs
Transaction rate:              87.09 trans/sec
Throughput:                     1.77 MB/sec
Concurrency:                   95.22
Successful transactions:        5148
Failed transactions:               0
Longest transaction:           13.22
Shortest transaction:           0.00
```

200 threads
```bash
Transactions:                   5147 hits
Availability:                 100.00 %
Elapsed time:                  59.57 secs
Data transferred:             104.52 MB
Response time:                  2.08 secs
Transaction rate:              86.40 trans/sec
Throughput:                     1.75 MB/sec
Concurrency:                  180.05
Successful transactions:        5148
Failed transactions:               0
Longest transaction:           27.40
Shortest transaction:           0.00
```

500 threads
```bash
Transactions:                   1245 hits
Availability:                  49.42 %
Elapsed time:                  37.14 secs
Data transferred:              25.37 MB
Response time:                  2.41 secs
Transaction rate:              33.52 trans/sec
Throughput:                     0.68 MB/sec
Concurrency:                   80.77
Successful transactions:        1245
Failed transactions:            1274
Longest transaction:           36.35
Shortest transaction:           0.00
```

Summary:
```bash
+-----------------------------------+------------------+------------------+
| concurrency |   Transaction rate  |  Response Time   |  Availability (%)|
+-------------+---------------------+------------------+------------------+
|     10      |        70.25        |       0.14       |       100        |
+-------------+---------------------+------------------+------------------+
|     25      |        74.59        |       0.33       |       100        |
+-------------+---------------------+------------------+------------------+
|     50      |        73.64        |       0.66       |       100        |
+-------------+---------------------+------------------+------------------+
|    100      |        87.09        |       1.09       |       100        |
+-------------+---------------------+------------------+------------------+
|    200      |        86.40        |       9.08       |       100        |
+-------------+---------------------+------------------+------------------+
|    500      |        33.52        |       2.41       |      49.42       |
+-------------+---------------------+------------------+------------------+
```

As we can see, availability is 100% for all the tests. Though, the response time constantly increases. For more than 100 concurrent requests, it becomes unacceptable (9 seconds for 200 requests). For 500 requests the server is available for only ~50% of requests.

## Load Testing with probabilistic cache flushing

0. Check the CPU, Mongo and Redis metrics for the regular configuration (with no cache flushing) (for 200 threads):

![dashboard](img/01_initial_state.png)

1. Test with no cache flushing:
```bash
siege -f urls.txt -R siege.conf -t60s -i -v -c 200

Transactions:                   4132 hits
Availability:                 100.00 %
Elapsed time:                  59.39 secs
Data transferred:              83.25 MB
Response time:                  2.60 secs
Transaction rate:              69.57 trans/sec
Throughput:                     1.40 MB/sec
Concurrency:                  181.04
Successful transactions:        4133
Failed transactions:               0
Longest transaction:           34.72
Shortest transaction:           0.00
```
![dashboard](img/02_before_caching.png)

2. Turn on Probabilistic cache (set `PROBABILISTIC_CACHE_FLUSHING=true`) in `.env` file and test within the same concurrency:

```bash
siege -f urls.txt -R siege.conf -t60s -i -v -c 200

Transactions:                   4166 hits
Availability:                 100.00 %
Elapsed time:                  59.78 secs
Data transferred:              84.53 MB
Response time:                  2.52 secs
Transaction rate:              69.69 trans/sec
Throughput:                     1.41 MB/sec
Concurrency:                  175.89
Successful transactions:        4167
Failed transactions:               0
Longest transaction:           33.81
Shortest transaction:           0.00
```
![dashboard](img/03_after_caching.png)

Our default ttl is 60 seconds, we start probabilisticly flush the cache 30 seconds before cache expiration. That is why on the dashboard we see only one query for `MongoDB` and increased reads number from `Redis`. Cache was re-written based on probabilistic cache expiration algorithm.

### Useful Commands
Save the results from log file:
```bash
grep -e "\*\*\*" ../shared/blog/log/development.log > results.txt
```

## References
- [Siege Repo](https://github.com/JoeDog/siege/)
- [Siege Options](https://www.linode.com/docs/guides/load-testing-with-siege/)
- [Mastering Low Level Caching in Rails](https://www.honeybadger.io/blog/rails-low-level-caching/)
- [Cache Stampede](https://en.wikipedia.org/wiki/Cache_stampede)
- [Optimal Probabilistic Cache Stampede Prevention](https://cseweb.ucsd.edu/~avattani/papers/cache_stampede.pdf)
