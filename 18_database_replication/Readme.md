# 18. Database Replication
## Assignment
Set up MySQL Cluster.
1. Create 3 docker containers: mysql-m, mysql-s1, mysql-s2
2. Setup master slave replication (Master: mysql-m, Slave: mysql-s1, mysql-s2)
3. Write script that will frequently write data to database
4. Ensure, that replication is working
5. Try to turn off mysql-s1,
6. Try to remove a column in database on slave node

## 1-2-3. Set up MySQL Cluster

Start the nodes so that they create initial configuration
```bash
docker-compose up mysql-m mysql-s1
```

Then start all services, including `app` – the script which adds a new recond to the table `posts` every second.
```bash
docker-compose up
```

Grant privileges and lock the master (password: `root_pass_master`)
```bash
docker-compose exec mysql-m mysql -u root -P 3306 -D mydb -p

GRANT REPLICATION SLAVE ON *.* TO 'app_replica'@'%' IDENTIFIED BY 'pass_replica';
FLUSH PRIVILEGES;

USE mydb;
FLUSH TABLES WITH READ LOCK;

SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |    33743 | mydb         |                  |                   |
+------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

Now let's make a mysql dump (password: `root_pass_master`)

(in separate window, not to break the lock on Master)
```bash
docker-compose exec mysql-m bash

mkdir -p /app
mysqldump -u root -P 3306 -p mydb > /app/mydb.sql
```

Release writing lock on master:
```bash
docker-compose exec mysql-m mysql -u root -D mydb -P 3306 -p

UNLOCK TABLES;
> Query OK, 0 rows affected (0.00 sec)
```

Configure Replica database (password: `root_pass_replica`)
```sql
docker-compose exec mysql-s1 bash

mysql -u root -p mydb < /app/mydb.sql

mysql -u root -D mydb -p

 select * from posts order by id desc limit 5;
+-----+--------------------------+-------------+--------------------+
| id  | name                     | description | price              |
+-----+--------------------------+-------------+--------------------+
| 935 | name-0.44561408631950583 | some        | 44.561408631950584 |
| 934 | name-0.02894742118728999 | some        |  2.894742118728999 |
| 933 | name-0.2802492711003636  | some        |  28.02492711003636 |
| 932 | name-0.625820667941809   | some        |   62.5820667941809 |
| 931 | name-0.12113526558444732 | some        | 12.113526558444732 |
+-----+--------------------------+-------------+--------------------+

CHANGE MASTER TO MASTER_HOST='mysql-m', MASTER_USER='app_replica', MASTER_PASSWORD='pass_replica', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 33743;

START SLAVE;
```

## 4. Ensure the replication is working

Let's check slave status on mysql-s1 (password: `root_pass_replica`)

```sql
docker-compose exec mysql-s1 mysql -u root -D mydb -p

SHOW SLAVE STATUS\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: mysql-m
                  Master_User: app_replica
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 65983
               Relay_Log_File: mysql-relay-bin.000002
                Relay_Log_Pos: 32560
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
           Replicate_Do_Table:
       Replicate_Ignore_Table:
      Replicate_Wild_Do_Table:
  Replicate_Wild_Ignore_Table:
                   Last_Errno: 0
                   Last_Error:
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 65983
              Relay_Log_Space: 32767
              Until_Condition: None
               Until_Log_File:
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File:
           Master_SSL_CA_Path:
              Master_SSL_Cert:
            Master_SSL_Cipher:
               Master_SSL_Key:
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error:
               Last_SQL_Errno: 0
               Last_SQL_Error:
  Replicate_Ignore_Server_Ids:
             Master_Server_Id: 1
                  Master_UUID: 1abd9c94-76b5-11ec-b796-0242ac180002
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind:
      Last_IO_Error_Timestamp:
     Last_SQL_Error_Timestamp:
               Master_SSL_Crl:
           Master_SSL_Crlpath:
           Retrieved_Gtid_Set:
            Executed_Gtid_Set:
                Auto_Position: 0
         Replicate_Rewrite_DB:
                 Channel_Name:
           Master_TLS_Version:
```
As we can see, `Slave_IO_Running: Yes, Slave_SQL_Running: Yes, Slave_IO_State: Waiting for master to send event`.

Now let's check if the records are replicated:
```sql
select * from posts order by id desc limit 5;
+------+--------------------------+-------------+--------------------+
| id   | name                     | description | price              |
+------+--------------------------+-------------+--------------------+
| 1182 | name-0.6129259230957802  | some        |  61.29259230957802 |
| 1181 | name-0.1534763395840678  | some        | 15.347633958406782 |
| 1180 | name-0.9752702693260684  | some        |  97.52702693260684 |
| 1179 | name-0.44647661397646266 | some        |  44.64766139764627 |
| 1178 | name-0.7044795506914281  | some        |  70.44795506914281 |
+------+--------------------------+-------------+--------------------+
```

They are replicated. Therefore, we can configure the second slave node:
(password: `root_pass_replica`)
```sql
docker-compose exec mysql-s2 bash

mysql -u root -p mydb < /app/mydb.sql

mysql -u root -D mydb -p

CHANGE MASTER TO MASTER_HOST='mysql-m', MASTER_USER='app_replica', MASTER_PASSWORD='pass_replica', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 33743;

START SLAVE;

select * from posts order by id desc limit 5;
+------+--------------------------+-------------+--------------------+
| id   | name                     | description | price              |
+------+--------------------------+-------------+--------------------+
| 1776 | name-0.9685476378005033  | some        |  96.85476378005033 |
| 1775 | name-0.5036359769239779  | some        |  50.36359769239779 |
| 1774 | name-0.21812800357978845 | some        | 21.812800357978844 |
| 1773 | name-0.5784250805915897  | some        |  57.84250805915897 |
| 1772 | name-0.947735394683469   | some        |   94.7735394683469 |
+------+--------------------------+-------------+--------------------+
```

Now two slave nodes replicate the records updates from master's binlog.

## 5. Try to turn off mysql-s1,
Let's try to turn of `mysql-s1`

```bash
docker-compose stop mysql-s1
```

On `mysql-s2` the records are still being added:
```bash
select * from posts order by id desc limit 5;
+------+---------------------------+-------------+--------------------+
| id   | name                      | description | price              |
+------+---------------------------+-------------+--------------------+
| 2021 | name-0.9681392942752741   | some        |  96.81392942752741 |
| 2020 | name-0.8756913704514935   | some        |  87.56913704514935 |
| 2019 | name-0.005547509933528927 | some        | 0.5547509933528927 |
| 2018 | name-0.8572013336893136   | some        |  85.72013336893136 |
| 2017 | name-0.8768056113487875   | some        |  87.68056113487876 |
+------+---------------------------+-------------+--------------------+
```

Let's start `mysql-s1` again:
```bash
docker-compose up -d mysql-s1
```

and check if it picks the latest updates (password: `root_pass_replica`):

```sql
docker-compose exec mysql-s1 mysql -u root -D mydb -p

 select * from posts order by id desc limit 5;
+------+--------------------------+-------------+--------------------+
| id   | name                     | description | price              |
+------+--------------------------+-------------+--------------------+
| 2215 | name-0.2976623636090553  | some        |  29.76623636090553 |
| 2214 | name-0.35823889780910123 | some        |  35.82388978091012 |
| 2213 | name-0.24926619893511548 | some        |  24.92661989351155 |
| 2212 | name-0.7230442517969774  | some        |  72.30442517969774 |
| 2211 | name-0.31089971659311866 | some        | 31.089971659311864 |
+------+--------------------------+-------------+--------------------+
```

## 6. Try to remove a column in database on slave node

### Removing the last column
Let's remove last column (`price`) on `mysql-s1`:

```sql
ALTER TABLE posts DROP COLUMN price;

select * from posts order by id desc limit 2;
+------+-------------------------+-------------+
| id   | name                    | description |
+------+-------------------------+-------------+
| 2324 | name-0.7160153136563497 | some        |
| 2323 | name-0.2546483750457176 | some        |
+------+-------------------------+-------------+
```

As we can see, the records are still being added.

Let's put the column back
```
ALTER TABLE posts ADD COLUMN price DECIMAL NOT NULL;

select * from posts order by id desc limit 2;
+------+---------------------------+-------------+--------------------+
| id   | name                      | description | price              |
+------+---------------------------+-------------+--------------------+
| 2759 | name-0.003179825096155664 | some        | 0.3179825096155664 |
| 2758 | name-0.22875203008868672  | some        | 22.875203008868674 |
+------+---------------------------+-------------+--------------------+
```
The price replication is back.

Though, the records created during the time price column was removed, have zero (default) price.

```sql
 select * from posts where price = 0.0 order by id desc limit 2;
+------+--------------------------+-------------+-------+
| id   | name                     | description | price |
+------+--------------------------+-------------+-------+
| 2620 | name-0.7304028988112022  | some        |     0 |
| 2619 | name-0.17519912468745558 | some        |     0 |
+------+--------------------------+-------------+-------+
```

### Removing the column in the middle of the table

Let's remove last column (`description`) on `mysql-s2`:
```sql
ALTER TABLE posts DROP COLUMN description;

select * from posts order by id desc limit 2;
+------+-------------------------+--------------------+
| id   | name                    | price              |
+------+-------------------------+--------------------+
| 2913 | name-0.7683963872290421 |  76.83963872290421 |
| 2912 | name-0.5185814370994247 | 51.858143709942475 |
+------+-------------------------+--------------------+

SHOW SLAVE STATUS\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: mysql-m
                  Master_User: app_replica
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000002
          Read_Master_Log_Pos: 399087
               Relay_Log_File: mysql-relay-bin.000003
                Relay_Log_Pos: 387550
        Relay_Master_Log_File: mysql-bin.000002
             Slave_IO_Running: Yes
            Slave_SQL_Running: No
              Replicate_Do_DB:
          Replicate_Ignore_DB:
           Replicate_Do_Table:
       Replicate_Ignore_Table:
      Replicate_Wild_Do_Table:
  Replicate_Wild_Ignore_Table:
                   Last_Errno: 1677
                   Last_Error: Column 2 of table 'mydb.posts' cannot be converted from type 'varchar(255(bytes))' to type 'double'
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 387337
              Relay_Log_Space: 608464
              Until_Condition: None
               Until_Log_File:
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File:
           Master_SSL_CA_Path:
              Master_SSL_Cert:
            Master_SSL_Cipher:
               Master_SSL_Key:
        Seconds_Behind_Master: NULL
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error:
               Last_SQL_Errno: 1677
               Last_SQL_Error: Column 2 of table 'mydb.posts' cannot be converted from type 'varchar(255(bytes))' to type 'double'
  Replicate_Ignore_Server_Ids:
             Master_Server_Id: 1
                  Master_UUID: 1abd9c94-76b5-11ec-b796-0242ac180002
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State:
           Master_Retry_Count: 86400
                  Master_Bind:
      Last_IO_Error_Timestamp:
     Last_SQL_Error_Timestamp: 220116 11:15:44
               Master_SSL_Crl:
           Master_SSL_Crlpath:
           Retrieved_Gtid_Set:
            Executed_Gtid_Set:
                Auto_Position: 0
         Replicate_Rewrite_DB:
                 Channel_Name:
           Master_TLS_Version:
```
The replication is broken. There is an error `Column 2 of table 'mydb.posts' cannot be converted from type 'varchar(255(bytes))' to type 'double'`

## References
- [Common replication problems](http://www.mysql.ru/docs/man/Replication_Features.html)
- [Ruby Sequel](https://github.com/jeremyevans/sequel)

## Questions
1. How to make it work with MySQL 8.0?
