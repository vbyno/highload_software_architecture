require 'sequel'
require 'mysql2'
require 'pry'

sleep 10 # Wait for MySQL

# client = Mysql2::Client.new(:host => "mysql-m", :username => "app", password: 'pass_master', port: 3306, database: 'mydb')
DB = Sequel.connect('mysql2://app:pass_master@mysql-m:3306/mydb')

DB.create_table?(:posts) do
  primary_key :id
  String :name, null: false
  # If this column has the same type as the last one, row replication
  # might insert discription data in price column.
  String :description
  Float :price, null: false

  index :name
end

posts = DB[:posts]

10_000.times do
  sleep 1
  value = rand
  posts.insert(
    name: "name-#{value}",
    description: 'some',
    price: value * 100
  )
end

binding.pry
