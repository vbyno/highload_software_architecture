FROM ruby:3.1.0

RUN gem install sequel
RUN gem install mysql2
RUN gem install pry

WORKDIR /app
