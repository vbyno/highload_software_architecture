# 02. Resource Monitoring Systems

## Execution
```
cd 02_resource_monitoring_systems
```

1) `sudo chown $USER /var/run/docker.sock` – give permissin for telegram to read docker socket file
2) `docker-compose up` here and in `../shared/blog`
3) Go to http://localhost:8086 to register and create a token
4) Set parameters in `.env` file
5) `sh ./run.sh` - to upload graphana dashboard
6) Go to graphana: `http://localhost:3000` (admin/admin)

Metrics:
![dashboard](images/dashboard.png)

7) Run apache benchmark:
```bash
ab -k -c 10 -n 1000 http://localhost/posts
```

Metrics:
![dashboard](images/dashboard_under_load.png)


## Links
- [TIG Compose Repository](https://github.com/nicolargo/docker-influxdb-grafana)
- [InfluxDB Tokens](https://docs.influxdata.com/influxdb/v2.0/security/tokens/view-tokens/)
- [InfluxDB 2.0 and Telegraf Using Docker](https://www.influxdata.com/blog/running-influxdb-2-0-and-telegraf-using-docker/)
- [Apache Benchmarking tool](https://httpd.apache.org/docs/2.4/programs/ab.html)
- [Apache Benchmark Docker Container](https://hub.docker.com/r/jordi/ab)
