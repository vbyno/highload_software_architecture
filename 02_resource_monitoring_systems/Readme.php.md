## Links
- [TIG Compose Repository](https://github.com/nicolargo/docker-influxdb-grafana)
- [Laradock Compose Files](https://github.com/laradock/laradock/blob/master/docker-compose.yml)
- [How to configure laradock](https://medium.com/@bharatgupta_2334/how-to-develop-laravel-apps-with-laradock-locally-1329366eb352)
- [Laravel and MongoDB](https://www.mongodb.com/compatibility/mongodb-laravel-intergration)

## Execution
```
cd 02_resource_monitoring_systems
docher-compose up
```
Go to [http://localhost:3000](http://localhost:3000). Input creds: admin/admin and change the password.

Laradock
```
git clone https://github.com/Laradock/laradock.git
# https://laradock.io/
cd laradock
docker-compose up mongo elasticsearch nginx workspace
```
Go to [http://localhost](http://localhost)
cd laradock
docker-compose exec workspace bash
composer create-project --prefer-dist laravel/laravel laravel
cd laravel
composer install --ignore-platform-reqs
cp .env.example .env
#replace DB_NAME for mongo
php artisan serve
```

[Get Start With Mongo](https://docs.mongodb.com/mongoid/current/tutorials/getting-started-rails/)
```
sudo gem install rails -v '~> 6.1.4'
rails new blog --skip-active-record --skip-bundle
rails webpacker:install
```
