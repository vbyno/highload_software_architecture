#!/bin/bash

# Upload Dashboard to Grafana
grafanaDashboard=$(<./dashboard.json)

curl -X POST http://admin:admin@localhost:3000/api/dashboards/db \
     -H 'Accept: application/json' \
     -H 'Content-Type: application/json' \
     -d "{\"dashboard\":$grafanaDashboard}"

echo "Dashboard created"

# Upload Datasource to Grafana
grafanaDatasource=$(<./datasource.json)
curl -X POST http://admin:admin@localhost:3000/api/datasources \
     -H 'Accept: application/json' \
     -H 'Content-Type: application/json' \
     -d "$grafanaDatasource"
