require 'set'

class NodeContext
  attr_reader :parent, :node, :relation

  def initialize(parent, node, relation)
      @parent = parent
      @node = node
      @relation = relation
  end
end

class TreeNode
  attr_accessor :value, :left, :right, :parent

  def initialize(value)
      @value = value
      @left = nil
      @right = nil
      @parent = nil
  end

  # def set_left(node)
  #   self.left = node
  #   node.parent = self

  def left_or_right_node(v)
    raise ArgumentError, "value equals" if v == value

    v < value ? left : right
  end

  def children_count
    [left, right].compact.count
  end

  def set(relation, value)
    relation == :left ?  self.left = value : self.right = value
  end

  def to_s
    "(#{left.to_s}, #{value}, #{right.to_s})"
  end

  def to_a
    [*Array(left&.to_a), value, *Array(right&.to_a)]
  end
end

class BST
  attr_accessor :root, :size, :set

  def initialize
    @root = nil
    @size = 0;
    @set = Set.new
  end

  def insert(value)
    set.add(value)
    if root.nil?
      self.root = TreeNode.new(value)
      return self.size += 1
    end

    curr_node = previous_node = root

    while curr_node && curr_node.value != value
      previous_node = curr_node
      curr_node = curr_node.left_or_right_node(value)
    end

    return size if curr_node # already exists

    if value < previous_node.value
      previous_node.left = TreeNode.new(value)
    else
      previous_node.right = TreeNode.new(value)
    end

    self.size += 1
  end

  def find(value)
  end

  def contains?(value, node = self.root)
    if node == nil
      return false
    elsif value < node.value
      return contains?(value, node.left)
    elsif value > node.value
      return contains?(value, node.right)
    else
      return true
    end
  end

  def find_min(node = root, parent = root, direction = :left)
    current_node = node

    while current_node&.left do
      parent = current_node
      direction = :left
      current_node = current_node.left
    end

    NodeContext.new(parent, current_node, direction)
  end

  def closest_by_value(value, node = root)
    curr_node = previous_node = node
    direction = nil
    # while context.searchable?(value)

    while curr_node && curr_node.value != value
      previous_node = curr_node

      if value < curr_node.value
        curr_node = curr_node.left
        direction = :left
      else
        curr_node = curr_node.right
        direction = :right
      end
    end

    NodeContext.new(previous_node, curr_node, direction)
  end

  def delete(value)
    set.delete(value)
    context = closest_by_value(value)
    return if context&.node&.value != value
    delete_by_context(context)
  end

  def delete_by_context(context)
    if context.node && context.node.value == root.value
      delete_root(context)
    else
      delete_regular_node(context)
    end

    context.node
  end

  def delete_root(context)
    if root.left.nil? && root.right.nil?
      self.root = nil
    elsif root.left && root.right
      new_root = delete_by_context(find_min(root.right, root, :right))
      new_root.left = root.left
      new_root.right = root.right
      self.root = new_root
    else
      self.root = root.left || root.right
    end
  end

  def delete_regular_node(context)
    node = context.node
    parent = context.parent

    if node.left.nil? && node.right.nil?
      parent.set(context.relation, nil)
    elsif node.left && node.right
      parent.set(context.relation, delete_by_context(find_min(node.right)))
    else
      parent.set(context.relation, node.left || node.right)
    end
  end

  #in-order treaversal
  def to_s
    root.to_s
  end

  def to_a
    Array(root&.to_a)
  end

  def to_set_a
    set.to_a.sort
  end
end
