require 'rspec'
require_relative 'bst'

RSpec.describe BST do
  let(:bst) { described_class.new }

  describe '#insert' do
    before do
      [10, 12, 16, 12].each { |n| bst.insert(n) }
    end

    it 'does not duplicate the key' do
      expect(bst.to_s).to eq "(, 10, (, 12, (, 16, )))"
    end
  end

  describe '#find_min' do
    def build(values)
      described_class.new.tap do |tree|
        Array(values).each { |n| tree.insert(n) }
      end
    end

    it 'finds minimal value' do
      expect(build([10, 5, 6, 1, 12]).find_min.node.value).to eq 1
    end

    it 'finds minimal value' do
      expect(build([1]).find_min.node.value).to eq 1
    end
  end

  describe '#delete' do
    before do
      [10, 5, 6, 1, 12].each { |n| bst.insert(n) }
    end

    specify do
      expect { bst.delete(12)}.to change { bst.to_s }.to('(((, 1, ), 5, (, 6, )), 10, )')
    end

    specify do
      expect { bst.delete(5)}.to change { bst.to_s }.to('((, 6, ), 10, (, 12, ))')
    end

    specify do
      expect { bst.delete(10)}.to change { bst.to_s }.to('(((, 1, ), 5, (, 6, )), 12, )')
    end
  end

  describe 'all operations' do
    it 'performs' do
      100000.times do |i|
        bst.insert(rand(1000))
        bst.delete(rand(1000))
      end
      puts bst.to_s
      print bst.to_a
      print bst.to_set_a
      expect(bst.to_a).to eq (bst.to_set_a)
    end
  end
end
