require 'set'
require_relative 'node'

module BstRecursive
  class Tree
    attr_accessor :root, :size, :set

    def initialize
      @root = nil
      @size = 0;
      @set = Set.new
    end

    def insert(value)
      set.add(value)

      if root.nil?
        self.root = Node.new(value)
        return self.size += 1
      end

      root.insert(value)
    end

    def delete(value)
      set.delete(value)

      return unless root

      if root.value == value
        self.root = root.delete_itself
      else
        root.delete(value)
      end
    end

    def contains?(value)
      return false unless root

      root.contains?(value)
    end

    def min
      root.min.value if root
    end

    def to_s
      root.to_s
    end

    def to_a
      Array(root&.to_a)
    end

    def to_set_a
      set.to_a.sort
    end
  end
end
