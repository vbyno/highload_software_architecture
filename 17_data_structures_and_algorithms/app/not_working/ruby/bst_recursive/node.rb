module BstRecursive
  class Node
    attr_accessor :value, :left, :right

    def initialize(value)
      @value = value
      @left = nil
      @right = nil
    end

    def insert(v)
      return if v == value

      if v < value
        left.nil? ? self.left = self.class.new(v) : left.insert(v)
      else
        right.nil?? self.right = self.class.new(v) : right.insert(v)
      end
    end

    def delete(v)
      raise ArgumentError if v == value

      if left && v < value
        v == left.value ? (self.left = left.delete_itself) : left.delete(v)
      elsif right && v > value
        v == right.value ? (self.right = right.delete_itself) : right.delete(v)
      end
    end

    def min
      left ? left.min : self
    end

    def contains?(v)
      return true if v == value

      if v < value
        !!left && left.contains?(v)
      else
        !!right && right.contains?(v)
      end
    end

    def delete_itself
      if left.nil? && right.nil?
        nil
      elsif left && right
        right.min.tap do |min_right|
          if min_right.value != right.value
            right.delete(min_right.value)
            min_right.right = right
          end
          min_right.left = left
        end
      else
        left || right
      end
    end

    def to_s
      "(#{left.to_s}, #{value}, #{right.to_s})"
    end

    def to_a
      [*Array(left&.to_a), value, *Array(right&.to_a)]
    end
  end
end
