require 'rspec'
require_relative 'tree'

RSpec.describe BstRecursive::Tree do
  let(:bst) { build }

  def build(*values)
    described_class.new.tap do |tree|
      Array(values).each { |n| tree.insert(n) }
    end
  end

  describe '#insert' do
    it 'does not duplicate the key' do
      expect(build(10, 12, 16, 12).to_s).to eq "(, 10, (, 12, (, 16, )))"
    end
  end

  describe '#min' do
    specify { expect(build(10, 5, 6, 1, 12).min).to eq 1 }
    specify { expect(build(1).min).to eq 1 }
    specify { expect(build().min).to eq nil }
  end

  describe '#delete' do
    before do
      [10, 5, 6, 1, 12].each { |n| bst.insert(n) }
    end

    specify do
      expect { bst.delete(12)}.to change { bst.to_a }.to [1, 5, 6, 10]
    end

    specify do
      expect { bst.delete(12)}.to change { bst.to_s }.to ('(((, 1, ), 5, (, 6, )), 10, )')
    end

    specify do
      expect { bst.delete(5)}.to change { bst.to_a }.to [1, 6, 10, 12]
    end

    specify do
      expect { bst.delete(10)}.to change { bst.to_s }.to('(((, 1, ), 5, (, 6, )), 12, )')
    end
  end

  describe '#contains?' do
    specify { expect(build(12,5,2,6,111,202).contains?(2)).to be true }

    specify { expect(build(12,5,2,6,111,202).contains?(3)).to be false }
  end

  describe 'all operations' do
    it 'performs' do
      100000.times do |i|
        bst.insert(rand(1000))
        bst.delete(rand(1000))
      end
      expect(bst.to_a).to eq (bst.to_set_a)
    end
  end
end
