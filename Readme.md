# Course Syllabus
01. Resources and their limitations
02. [Resource monitoring systems](02_resource_monitoring_systems/Readme.md)
03. [Monitoring systems for user metrics](03_monitoring_systems_for_user_metrics)
04. [Stress Testing. Approaches and tools](04_stress_testing)
05. [Mathematical methods of analysis and forecasting](05_mathematical_methods_of_analysis)
06. [High Load Applications Architecture](06_high_load_applications_architecture)
07. [Web Servers](07_web_servers)
08. SQL Databases
    - [Fine Tuning and Optimization](08_0_sql_databases_fine_tuning_and_optimization)
    - [Transactions, Isolations, Locks](08_1_transactions_isolations_locks)
09. NoSQL Databases
    - [ElasticSearch](09_0_nosql_databases_elasticsearch)
    - [Redis](09_1_nosql_databases_redis)
10. [Queues](10_queues)
11. [Content Delivery Network](11_content_delivery_network)
12. [Balancing](12_balancing)
13. [Logging](13_logging)
14. [DDoS Attacks](14_ddos_attacks)
15. [Peak Loading](15_peak_loading)
16. Storages Physics: HDD, SSD, RAM
17. [Data structures and Algorithms](17_data_structures_and_algorithms)
18. [Database: Replication](18_database_replication)
19. [Database: Sharding](19_database_sharding)
20. Backups
21. [Profiling](21_profiling)
22. [Continuous Deployment](22_continuous_deployment)
23. [AWS: EC2 and Load Balancer](23_aws_ec2_and_load_balancer)
24. [AWS: S3](24_aws_s3)
25. [AWS: Autoscale groups](25_aws_autoscale_groups)
26. [AWS: Serverless calculations](26_aws_lambda)
27. Art of Trade-Offs. ATAM
28. Building and Managing Tech Teams

# References
- [Course Landing Page](https://prjctr.com/course/highload-software-architecture)
