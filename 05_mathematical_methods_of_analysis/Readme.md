# 05. Mathematical methods of analysis

## Assignment
GitHub Financial Model
Adopt financial model, to GitHub:
1. Find key financial metrics for GitHub
2. Estimate Cost of one Paid User
3. Calculate ARPU (average revenue per user)
4. Find out when model reached break even

## 1-2-3. Key financial metrics and costs for GitHub
### Price per user per month
GitHub has [two plans](https://github.com/pricing). Team ($40 per user/month) and enterprise ($210 per user/month). Although, we don't know the number of customers.

GitHub does not publicly disclose its earnings. But, at the time of the Microsoft acquisition in 2018, GitHub had reported revenues of $200 to $300 million. [zoominfo](https://www.zoominfo.com/c/github-inc/345645238) states its revenue is $378 Million now, but it is strange, since Github significantly improved its userbase since 2018. Let's take $250M for $28M users, as a state for 2018 year.

Therefore, price per user per month is $250 Million / 12 / 28 Million = *$0.744*. Here we assume that one new user brings 0.744 to monthly revenue regardless of having the paid subscription, or not. It seems reasonable to suppose this.

### Banking fees and commissions
Let's set it to 3%

### Personal Income Tax
Personal Income Tax Rate in the United States averaged *36.71 percent* from 2004 until 2020

### Corporate Income Tax
21% – corporate income tax USA

### User acquisition Cost
According to [these statistics](https://firstpagesage.com/seo-blog/seo-roi/average-customer-acquisition-cost-cac-by-industry-b2b-edition-fc/), user acquisition for B2B SaaS is $205. Since in our model we rely on all users, not the ones who pay, we may take 1% of this cost (assuming that customers are 1% of the whole users amount). So, let's set `$2` as user acquisition cost. We will calculate the marketing expenses based on this cost and the number of new users each month.

### Server Monthly price
The most critical resource for the repository is storage.
GitHub suggests keeping repository size to between 100MB and 300MB. For instance, Git itself is 222MB. We will set the average repository size as 100mb to take into account the huge number of rarely updated repositories.

According to [AWS pricing](https://aws.amazon.com/ec2/pricing/on-demand/), AWS EBS instance volume costs *$0.10 per GB-month* of provisioned storage.

Github's competitor had [45 severs](https://about.gitlab.com/blog/2016/04/29/look-into-gitlab-infrastructure/) in 2016, which served 2m+ users.

### Users on one server
Taking 100mb as an average repo size and knowing that there are 73 mission repos and 56 million users, we can calculate:

```bash
x = 73m / (0.1Gb * 200m) = 3.65
```

So, 3.65 users will use 1 GB of storage on a monthly basis.

However, parallel requests will load CPU. Hence, we need at least $500-server. It will satisfy the needs of 10K users, since the Git-based system usage is moderate.

### Start date
GitHub was founded in 2008. We will take 1st Jan 2009 as a start date.

### Rent price per sq m
San Francisco office price per square foot in 2020 was $87.18 on average. Meaning, it is $87.18 * 0.092903 = $8.1 per square meter.

### Min square per employee sq m
[65%](https://remote.co/company/github-inc/) of Github employees work remotely. Therefore, we can calculate min square meters per employee as 10 * 0.35 = *3.5*

## Total Users
Within the first 6 months of operation, GitHub stored over 10,000 open source projects on its website. Therefore, it had 10000*73/200 = 3650 users. In July 2012, Andreessen Horowitz invested a whopping $100 million into the firm. That same year, GitHub crossed the 2 million developer mark.

We know some checkpoints:
1. At the very first month, GitHub had 1000$ monthly revenue. Let's suppose it had 1000 users.

2. 6 months - 3650 users

3. 48 months (Jan, 2013) – 2 000 000 users

4. 90 months (Oct, 2016) - 5 800 000 users [link](https://octoverse.github.com/2016/)

5. 128 months (Oct, 2019) - 40 000 000 users [link](https://github.blog/2019-11-06-the-state-of-the-octoverse-2019)

4. 154 months (November 2021) – 73 000 000 users

I've tried to use [planetcalc](https://planetcalc.com/5992/) to approximate this with quadratic regression:

```bash
ax^2 + bx + c = N

where
a, b, c - coefficients
x – month number
N = number of users

Data:
X values
1 6 48 90 128 154
1000 3650 2000000 5800000 40000000 73000000
```
I was not satisfied with the results (were not accurate for the checkpoints).

Therefore, I've decided to have linear intervals to calculate intermediate values. Here is the plot displaying those numbers:

<img src="img/total_users_per_month.png" alt="Tatal users per month" width="500"/>

## Employees
According to [Github timeline](https://en.wikipedia.org/wiki/Timeline_of_GitHub):

- June 2011 (30 months) – 33 employees
- December 2012 (46 months) - 139 employees
- December 2013 (58 months) - 234 employees
- February 2015 (73 months) - 257 employees
- August 2015 (77 months) - 330 employees
- May 2016 (89 months) - 568 employees
- December 2016 (95 months) - 592 employees
- 2022 (156 months): The company employs over 2,000 people across 8 worldwide offices.

<img src="img/employees_by_month.png" alt="Tatal employees by month" width="500"/>

According to [Glassdoor](https://www.glassdoor.com/Salary/GitHub-Salaries-E671945.htm?filter.payPeriod=MONTHLY), the average salary for a software engineer at GitHub is $13,359/month.

However, inflation [took place since 2009](https://smartasset.com/investing/inflation-calculator). Moreover, [IT salary watch](https://www.computerworld.com/salarysurvey/breakdown/2009/joblevel/3) shows the avarage yearly salary was ~70-80K back in 2009. Therefore, we can set $6500 as a CEO salary and downgrade it to $4K for L1-level roles.

## 4. Find out when the model reached break even
According to [the model calsulations](https://docs.google.com/spreadsheets/d/1lLJrHVzYjTRo7xVbf79ZBlILKIiLYDS_MYa33xS4LjQ/edit#gid=541062416), GitHub didn't reach break-even in the first 5 years of its existence:

<img src="img/cost_revenue_profit.png" alt="Cost, revenue, profit" width="500"/>

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQeTv5jcPs2r19Y4Y9W8UUClbdR5LQOqPcKQ_fkm6HdfRgchRHnjw4r1QRSBmNS3ENGdyIFXZk2ZnzR/pubhtml?widget=true&amp;headers=false"></iframe>

We can explain it by the goals GitHub had. We noticed the ROI became positive in July 2011, but GitHub started to expand its personnel, which harmed the profits. As we can see, the strategy was to seize the market.

## References
- [GitHub financials](https://craft.co/github/metrics)
- [Financial Model Template](https://docs.google.com/spreadsheets/d/1C2S-bnvLNxzL3UNaIg8CCi0GU1h9MUxgXKFcBU3TxXc/edit)
- [Average Customer Acquisition Cost (CAC) By Industry](https://firstpagesage.com/seo-blog/seo-roi/average-customer-acquisition-cost-cac-by-industry-b2b-edition-fc/)
- [The GitHub Business Model – How Does GitHub Make Money?](https://productmint.com/github-business-model-how-does-github-make-money/)
- [crunchbase](https://www.crunchbase.com/organization/github)
- [Analysis](https://docs.google.com/spreadsheets/d/1lLJrHVzYjTRo7xVbf79ZBlILKIiLYDS_MYa33xS4LjQ/)

## Questions
