# 15. Peak Loading

## Assignment
Describe solution that solves peak loadings problem for biggest European football website https://goal.com

Analyze all types of pages on the site
Analyze and list possible sources of peak loadings
Describe possible solutions for each type

## 1. Analyze all types of pages on the site
There are several types of pages:

### Landing pages
<img src="img/page_main.png" alt="" width="600"/>

The main page, `Transfers` and `Breaking News` are all blog-like pages containing text, photos, and links to external resources (youtube or Twitter videos).

### Live Scores Pages
<img src="img/page_scores.png" alt="" width="600"/>

This page shows match scores in real-time. One could select any date and observe the results of matches.

### Statistical pages
<img src="img/stats_overview.png" alt="" width="600"/>

`Team info`, `Player info`, `Championship`, `Standings` are the statistical tables that update numbers from time to time. They are similar to Live Scores pages, but they are more tolerant of delays.

### Push notifications for mobile app
<img src="img/mobile_app.png" alt="" width="600"/>

Those pages notify about scores in real-time.

## 2. Analyze and list possible sources of peak loadings
According to [similarweb.com](https://www.similarweb.com/website/goal.com/#traffic), the website has 69M users per month (as average, based on the last three months).

<img src="img/similarweb_statistics.png" alt="" width="600"/>

Avg visit duration = 1m54s

```bash
69m / 30.5 / 24 / 60  = 1571 * 2 minutes (visit time) = 3142 concurrent users
```

Geography & Country targeting shows top countries where the traffic comes from are: Japan, USA, Indonesia, Italy, and Brazil

<img src="img/similarweb_geography.png" alt="" width="600"/>

Hence, we must adjust to the traffic spikes from those countries.

Marketing channels distribution states that 47% of traffic is direct traffic. Therefore, the website has many returning users.

<img src="img/similarweb_marketing.png" alt="" width="600"/>

## 3. Describe possible solutions for each type

All the images must be served and cached with CDN to be available in the countries of the most popular traffic.

To serve 3142 concurrent users all over the globe, we may have data centers in the USA, Brazil, Italy, Indonesia, and Japan. We would cover all the world. Doing that, we will optimize the network for the top-6 countries.

The app is ready-heavy. Therefore app server must not be a problem. We will heavily use cache servers instead.

### Landing pages
Those pages are static. Once written, they are rarely updated. Therefore, we can cache their content and then serve from the cache.

### Live Scores Pages
We will use short pooling to put all the responsibility on clients.

Every 5 seconds, clients will re-fetch the page from the cache if the page is active. To prevent DDoS attacks, we reduce the limit of Get-requests to 5 requests/second per client.

The cache won't have any TTL. We will always rewrite the existing cache with unlimited TTL if the data is updated.

### Statistical pages
They are re-calculated in the same way as Live Scores pages. Although, we will process the updates in a low-priority queue. The data is cached, as for Live Scores pages.

The high-level architecture looks like this:

<img src="img/architecture.png" alt="" width="600"/>
