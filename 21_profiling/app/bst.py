from __future__ import print_function
from pybst.rdbtree import RBTree
import pdb
import random
import cProfile, pstats
import sys

def insert(bst, array):
    for value in array:
        bst.insert(value, value)

def search(bst, n, limit):
    for _ in range(n):
        value = random.randint(0, limit)
        bst.get_node(value)

def test_delete(bst, n, limit):
    for i in range(n):
        value = random.randint(0, limit)
        bst.delete(value)

    # assert(1 == 1)

if __name__ == "__main__":
    bst = RBTree()
    unique_values_number = int(sys.argv[1])
    search_iterations = 10000
    n_experiments = 1
    insert_durations = []
    search_durations = []

    # cProfile.run('test_delete(bst, n, limit)')
    for i in range(n_experiments):
        array = range(unique_values_number)
        random.shuffle(array)

        insert(bst, array)

        search(bst, search_iterations, unique_values_number)
