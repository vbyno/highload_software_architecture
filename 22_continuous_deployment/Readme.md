# 22. Continuous Deployment

## Assignment
Setup CI/CD for the project:

Unit testing
Platform testing
Deliver to Staging
Acceptance testing
Deploy to Production
Post Deploy testing

## Solution
We've aded [.gitlab-ci.yml](../.gitlab-ci.yml) file at the root of the project.

To test locally:
```ruby
docker-compose up
terraform init
terraform apply

ssh -i ~/.ssh/aws_key ec2-user@$(terraform output --raw ec2_ip)
```

It has such stages:

<img src="img/deployment_plan.png" width="600"/>

The image is built at the first stage, and all the other operations are done with this image. Eventually EC2 instance is replaced with a new one which fetches the image with `latest` tag.

Sensitive variables are added to the project's settings:

<img src="img/env_variables.png" width="600"/>

If any stage fails, the whole pipeline fails:

<img src="img/failed_pipeline.png" width="600"/>

## References
- [Cuba Ruby Server](https://github.com/soveran/cuba)

## Questions
