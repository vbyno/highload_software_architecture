require 'rspec'
require_relative 'capybara_helper'

RSpec.describe "the signin process", type: :feature do
  it "returns expected text" do
    visit '/'
    expect(page).to have_content 'Hello, World'
  end
end
