
#!/bin/bash

url=$1
text=$(curl "$url")
expected_text='Hello, World!'

if [ "$text" = "$expected_text" ]; then
  echo "Expected response!"
else
  echo "Incorrect response: $text"
  exit 1
fi
