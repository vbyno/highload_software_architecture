require 'rspec'
require_relative '../hello_world'

RSpec.describe HelloWorld do
  subject(:call) { described_class.call }

  it 'returns expected text' do
    expect(call).to eq 'Hello, World!'
  end
end
