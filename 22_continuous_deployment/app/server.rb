# require "cuba"
# # require "cuba/safe"
# require_relative 'hello_world'

# Cuba.use Rack::Session::Cookie, :secret => "__a_very_long_string__"
# # Cuba.plugin Cuba::Safe

# Cuba.define do
#   on get do
#     on "hello" do
#       res.write HelloWorld.call()
#     end

#     on root do
#       res.redirect "/hello"
#     end
#   end
# end

require 'sinatra'
require_relative 'hello_world'

set :port, 3000
set :bind, '0.0.0.0'

get '/' do
  HelloWorld.call()
end
