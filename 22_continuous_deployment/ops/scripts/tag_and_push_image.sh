#!/bin/bash

image_tag=$1
timestamp=$(date +%Y_%m_%d_%H_%M_%S)
tag="$image_tag:$timestamp"
latest_tag="$image_tag:latest"

docker build . --tag "$tag"
docker image tag "$tag" "$latest_tag"
docker image push "$tag"
docker image push "$latest_tag"
