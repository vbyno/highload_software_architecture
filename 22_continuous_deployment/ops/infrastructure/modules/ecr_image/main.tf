resource aws_ecr_repository repo {
 name = "${var.name_prefix}-ecr-image"
}

resource null_resource ecr_image {
 depends_on = [aws_ecr_repository.repo]

  triggers = {
    docker_file = md5(file("${var.src}/Dockerfile"))
    dir_sha1 = md5(join("", [for f in fileset("${var.src}/app/", "*"): md5(file("${var.src}/app/${f}"))]))
  }

  provisioner "local-exec" {
    command = <<EOF
      aws ecr get-login-password --region ${var.aws_region} | docker login --username AWS --password-stdin ${aws_ecr_repository.repo.registry_id}.dkr.ecr.${var.aws_region}.amazonaws.com
      cd ${var.src}
      sh ops/scripts/tag_and_push_image.sh ${aws_ecr_repository.repo.repository_url}
    EOF
  }
}
