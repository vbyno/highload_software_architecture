variable "name_prefix" {
  type = string
  description = "Name prefix of the infrastructure elements"
}

variable "aws_region" {
  type        = string
  description = "AWS Region for lambdas"
  default     = "eu-west-3"
}

variable "src" {
  type        = string
  description = "An absolute path to the root directory containing /app and Dockerfile"
}
