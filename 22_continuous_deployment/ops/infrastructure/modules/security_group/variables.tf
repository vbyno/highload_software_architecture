variable "name_prefix" {
  type = string
  description = "Name prefix"
}

variable "vpc_id" {
  type = string
  description = "VPC"
}

variable "my_public_ip" {
  type = string
}
