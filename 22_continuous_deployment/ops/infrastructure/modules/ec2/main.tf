locals {
  sorted_subnet_ids = zipmap(
    tolist(range(var.instances_number)),
    [for i in range(var.instances_number): element(sort(var.subnet_ids), i)]
  )
}

resource "aws_instance" "ec2_instance" {
  subnet_id = local.sorted_subnet_ids[0]
  vpc_security_group_ids = var.security_group_ids

  launch_template {
    id      = var.launch_template_id
    version = "$Latest"
  }

  lifecycle {
    ignore_changes = [user_data]
  }

  tags = {
    Name = var.name_prefix
  }
}

resource "aws_eip" "default" {
  instance = aws_instance.ec2_instance.id
  vpc      = true
}
