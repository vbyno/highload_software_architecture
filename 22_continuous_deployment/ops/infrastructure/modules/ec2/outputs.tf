output "public_ip" {
  value = aws_eip.default.public_ip
}

output "ec2_instance_id" {
  value = aws_instance.ec2_instance.id
}
