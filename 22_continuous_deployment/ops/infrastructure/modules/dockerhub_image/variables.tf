variable "image_tag" {
  type = string
}

variable "src" {
  type        = string
  description = "An absolute path to the root directory containing /app and Dockerfile"
}
