resource null_resource image {
  triggers = {
    docker_file = md5(file("${var.src}/Dockerfile"))
    dir_sha1 = md5(join("", [for f in fileset("${var.src}/app/", "*"): md5(file("${var.src}/app/${f}"))]))
  }

  provisioner "local-exec" {
    command = <<EOF
      docker login
      cd ${var.src}
      sh ops/scripts/tag_and_push_image.sh ${var.image_tag}
    EOF
  }
}
