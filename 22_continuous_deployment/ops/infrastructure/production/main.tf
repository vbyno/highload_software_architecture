terraform {
  backend "s3" {
    bucket = "terraform-highload-state"
    key    = "22_continuous_deployment/live"
    region = "eu-west-3"
  }
}

provider "aws" {
  region = var.aws_region
}

data "http" "my_public_ip" {
  url = "http://ipv4.icanhazip.com"
}

data "aws_availability_zones" "available" {
  state = "available"
}

# module "dockerhub_image" {
#   source = "../modules/dockerhub_image"

#   src             = abspath("../../../")
#   repository_name = var.repository_name
# }

module "aws_vpc" {
  source = "../modules/vpc"

  name = var.name
  cidr_block = "10.1.0.0/16"
  availability_zones = data.aws_availability_zones.available.names
}

module "aws_ec2" {
  source = "../modules/ec2"

  name_prefix        = var.name
  instances_number   = 1
  subnet_ids         = module.aws_vpc.subnet_ids
  launch_template_id = module.aws_launch_template.launch_template_id
  security_group_ids = [module.aws_security_group.security_group_id]
}

module "aws_security_group" {
  source = "../modules/security_group"

  name_prefix = var.name
  vpc_id      = module.aws_vpc.id
  my_public_ip = chomp(data.http.my_public_ip.body)
}

module "aws_launch_template" {
  source = "../modules/launch_template"

  name_prefix              = var.name
  instance_type            = "t3.micro"
  ssh_local_key_path       = "~/.ssh/aws_key"
  vpc_id                   = module.aws_vpc.id
  my_public_ip             = chomp(data.http.my_public_ip.body)
  image_tag                = var.image_tag
  nginx_conf_path          = abspath("../../nginx.conf")
  docker_compose_path      = abspath("../../scripts/docker-compose.production.yml.tftpl")

  assigned_security_groups = [
    module.aws_security_group.security_group_id
  ]
}
