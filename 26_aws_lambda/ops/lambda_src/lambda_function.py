from __future__ import print_function
import boto3
import os
import sys
import uuid
import json
import io
import base64

from PIL import Image

s3_client = boto3.client('s3')

def convert_image(original_path, converted_path):
    with Image.open(original_path) as image:
        image.save(converted_path)

def lambda_handler(event, context):
    if event['httpMethod'] != 'POST':
        return None

    bucket = os.environ.get('bucket')
    image_id = uuid.uuid4()
    file_content = base64.b64decode(event['body'])
    download_path = '/tmp/{}.{}'.format(image_id, "jpeg")

    Image.open(io.BytesIO(file_content)).save(download_path)

    images = {}

    for image_format in ["png", "bmp", "gif"]:
        temp_path = '/tmp/{}.{}'.format(image_id, image_format)
        convert_image(download_path, temp_path)

        upload_path = '{}/{}.{}'.format(image_format, image_id, image_format)
        s3_client.upload_file(temp_path, bucket, upload_path)
        images[image_format] = '{}/{}'.format(bucket, upload_path)

    return {
        "statusCode": 200,
        "headers": { "Content-Type": "application/json" },
        "body": json.dumps(images)
    }
