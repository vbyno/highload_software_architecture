#!/bin/bash

# ecr_repository=$ECR_LAMBDA_REPOSITORY
ecr_repository=$1
timestamp=$(date +%Y_%m_%d_%H_%M_%S)
tag="$ecr_repository:$timestamp"
latest_tag="$ecr_repository:latest"

docker build . --tag "$tag"
docker image tag "$tag" "$latest_tag"
docker image push "$tag"
docker image push "$latest_tag"
