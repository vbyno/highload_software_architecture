terraform {
  backend "s3" {
    bucket = "terraform-highload-state"
    key    = "26_aws_lambda/live"
    region = "eu-west-3"
  }
}

provider "aws" {
  region = var.aws_region
}

module "aws_s3" {
  source = "../../shared/terraform_modules/s3_regular"

  name_prefix = "highloadcourse"
  aws_region  = var.aws_region
}

module "aws_lambda" {
  source = "../../shared/terraform_modules/lambda"

  name_prefix = "highloadcourse"
  aws_region  = var.aws_region
  src = abspath("lambda_src")
  description = "Lambda to convert JPEG to 3 other formats"
  s3_bucket_arn = module.aws_s3.bucket_arn
  s3_bucket_name = module.aws_s3.bucket_name
}

module "aws_api_gateway" {
  source = "../../shared/terraform_modules/api_gateway"

  name_prefix          = "highloadcourse"
  aws_region           = var.aws_region
  lambda_invoke_arn    = module.aws_lambda.invoke_arn
  lambda_function_name = module.aws_lambda.function_name
}
