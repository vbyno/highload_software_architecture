# 26. AWS. Lambda

## Assignment
Create Lambda function that will convert JPEG to BMP, GIF, PNG

## Solution
To find a reference Lambda architecture, we can go to Lambda -> Create Function -> Browse serverless app repository.

There we find two links:
- `applications/image-magick-lambda-layer`
- `applications/png-to-jpeg-conversion`

The architecture of the implementation looks like this:

<img src="img/architecture.png" alt="" width="600"/>

We send image to Api-Gateway, it bypasses it to Lambda function. Lambda converts the image to `bmp`, `gif` and `png` formats and saves all the images in the S3 bucket.
Let's provision the infrastructure:

```bash
# if needed
# asdf plugin-add terraform https://github.com/asdf-community/asdf-hashicorp.git
# asdf install
cd ops

terraform init
terraform apply
```

The terraform script builds an image and pushes it to AWS ECR repository. Then it launches Lambda, deploys API-Gateway and S3 bucket.

Api-Gateway:

<img src="img/api_gateway.png" alt="" width="600"/>

Lambda function looks like this:

<img src="img/lambda.png" alt="" width="600"/>

Let's upload jpeg image to API-Gateway:

```bash
curl -X POST -H 'Content-Type: image/jpeg' --data-binary @../img/image.jpeg "$(terraform output -raw api_gateway_base_url)" | jq .

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  9377  100   255  100  9122    104   3733  0:00:02  0:00:02 --:--:--  3838
{
  "png": "highloadcourse-regular-bucket/png/578fabc9-7d01-4ec4-a4d4-f7cb1f920dd8.png",
  "bmp": "highloadcourse-regular-bucket/bmp/578fabc9-7d01-4ec4-a4d4-f7cb1f920dd8.bmp",
  "gif": "highloadcourse-regular-bucket/gif/578fabc9-7d01-4ec4-a4d4-f7cb1f920dd8.gif"
}
```

As we can see, 3 images where created in the bucket. The bucket itself contains 3 folders:

<img src="img/bucket_overview.png" alt="" width="600"/>

Every folder contains the appropriate images:

<img src="img/bmp_folder.png" alt="" width="600"/>

Don't forget to destroy the infrastructure:
```bash
terraform destroy
```
## References
- [Serverless image handler](https://docs.aws.amazon.com/solutions/latest/serverless-image-handler/serverless-image-handler.pdf)
- [Python lambda function to resize images](https://github.com/harshawsharma/png-to-jpeg-conversion/blob/master/lambda_function.py)
- [How to Convert JPG to PNG using Python](https://datatofish.com/jpeg-to-png-python/)
- [Deploy Python Lambda functions with .zip file archives](https://docs.aws.amazon.com/lambda/latest/dg/python-package.html)
- [Terraform AWS Lambda](https://github.com/spring-media/terraform-aws-lambda/blob/v5.2.1/main.tf)
- [Getting started with AWS Lambda Layers for Python](https://medium.com/the-cloud-architect/getting-started-with-aws-lambda-layers-for-python-6e10b1f9a5d)
- [New for AWS Lambda – Container Image Support](https://aws.amazon.com/blogs/aws/new-for-aws-lambda-container-image-support/)
- [TERRAFORM – DEPLOY PYTHON LAMBDA (CONTAINER IMAGE)](https://hands-on.cloud/terraform-deploy-python-lambda-container-image/)
- [Upload binary files to S3 using AWS API Gateway with AWS Lambda](https://medium.com/swlh/upload-binary-files-to-s3-using-aws-api-gateway-with-aws-lambda-2b4ba8c70b8e)
