require 'benchmark'
require 'json'
require 'connection_pool'
require 'beaneater'
require 'redis'

n = 1_000
threads = 1

stream_name = 'test'

### Beanstalk #################################################################
beanstalk_publishers = threads.times.map { |i| Beaneater.new('localhost:11300') }
tube_publishers = ConnectionPool.new(size: threads) { beanstalk_publishers.shift.tubes[stream_name] }

beanstalk_subscriber = Beaneater.new('localhost:11300')
beanstalk_thread = Thread.new do
  beanstalk_subscriber.jobs.register(stream_name) do |job|
    raise(Beaneater::AbortProcessingError) if JSON.parse(job.body).fetch('n') >= n
  end

  puts "*"*100
  beanstalk_subscriber.jobs.process!
end

Benchmark.bm do |benchmark|
  benchmark.report("Beanstalkd - publish") do
    n.times.each_slice(threads) do |batch|
      batch
        .map do |i|
          Thread.new do
            tube_publishers.with { |conn| conn.put({n: i + 1}.to_json) }
          end
        end
        .each(&:join)
    end

    beanstalk_thread.join
  end

  # benchmark.report("Beanstalkd - subscribe") do
  #   beanstalk_thread.join
  # end
end

beanstalk_subscriber.close
beanstalk_publishers.each(&:close)

### Redis RDB #################################################################
redis_rdb_publisher = ConnectionPool.new(size: threads) { Redis.new(host: 'localhost', port: '6381', db: 1) }
redis_rdb_subscriber = Redis.new(host: 'localhost', port: '6381', db: 1)

redis_rdb_thread = Thread.new do
  # no competing subscribers :(
  redis_rdb_subscriber.subscribe(*stream_name) do |on|
    on.message do |channel, message|
      redis_rdb_subscriber.unsubscribe if JSON.parse(message).fetch('n') >= n
    end
  end
end

puts
Benchmark.bm do |benchmark|
  benchmark.report("Redis RDB - publish") do
    n.times.each_slice(threads) do |batch|
      batch
        .map do |i|
          Thread.new do
            redis_rdb_publisher.with { |conn| conn.publish(stream_name, {n: i + 1}.to_json) }
          end
        end
        .each(&:join)
    end

    redis_rdb_thread.join
  end

  # benchmark.report("Redis RDB - subscribe") do
  #   redis_rdb_thread.join
  # end
end

### Redis AOF #################################################################
redis_aof_publisher = ConnectionPool.new(size: threads) { Redis.new(host: 'localhost', port: '6382', db: 1) }
redis_aof_subscriber = Redis.new(host: 'localhost', port: '6382', db: 1)

redis_aof_thread = Thread.new do
  redis_aof_subscriber.subscribe(*stream_name) do |on|
    on.message do |channel, message|
      redis_aof_subscriber.unsubscribe if JSON.parse(message).fetch('n') >= n
    end
  end
end

puts
Benchmark.bm do |benchmark|
  benchmark.report("Redis AOF - publish") do
    # n.times { |i| redis_aof_publisher.publish(stream_name, {n: i + 1}.to_json) }
    n.times.each_slice(threads) do |batch|
      batch
        .map do |i|
          Thread.new do
            redis_aof_publisher.with { |conn| conn.publish(stream_name, {n: i + 1}.to_json) }
          end
        end
        .each(&:join)
    end

    redis_aof_thread.join
  end

  # benchmark.report("Redis AOF - subscribe") do
  #   redis_aof_thread.join
  # end
end

#### Nothing
puts
Benchmark.bm do |benchmark|
  benchmark.report("Nothing - publish") do
    n.times { |i| {n: i + 1}.to_json }
  end

  benchmark.report("Redis AOF - subscribe") do
  end
end
