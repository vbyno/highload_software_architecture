require 'benchmark'
require 'json'
require 'connection_pool'
require 'beaneater'
require 'redis'

N = 10_000
STREAM_NAME = 'test'

#### Nothing
puts
Benchmark.bm do |benchmark|
  benchmark.report("Nothing - publish") do
    N.times { |i| {n: i + 1}.to_json }
  end
end

def benchmark_redis(port, description)
  publisher = Redis.new(host: 'localhost', port: port, db: 1)
  subscriber = Redis.new(host: 'localhost', port: port, db: 1)

  thread = Thread.new do
    subscriber.subscribe(*STREAM_NAME) do |on|
      on.message do |channel, message|
        subscriber.unsubscribe if JSON.parse(message).fetch('n') >= N
      end
    end
  end

  puts
  Benchmark.bm do |benchmark|
    benchmark.report("#{description}   - publish") do
      N.times { |i| publisher.publish(STREAM_NAME, {n: i + 1}.to_json)}
    end

    benchmark.report("#{description} - subscribe") { thread.join }
  end
end

def benchmark_beanstalk
  publisher = Beaneater.new('localhost:11300')
  tube_publisher = publisher.tubes[STREAM_NAME]
  subscriber = Beaneater.new('localhost:11300')

  thread = Thread.new do
    subscriber.jobs.register(STREAM_NAME) do |job|
      raise(Beaneater::AbortProcessingError) if JSON.parse(job.body).fetch('n') >= N
    end

    subscriber.jobs.process!
  end

  Benchmark.bm do |benchmark|
    benchmark.report("Beanstalkd   - publish") do
      N.times { |i| tube_publisher.put({n: i + 1}.to_json) }
    end

    benchmark.report("Beanstalkd - subscribe") do
      thread.join
    end
  end

  subscriber.close
  publisher.close
end

benchmark_redis('6384', 'Redis - AOF')
benchmark_redis('6383', 'Redis - RDS')
benchmark_beanstalk
