# 10. Queues

## Assignment
Set up 3 containers - beanstalkd and redis (rdb and aof).
Write 2 simple scripts: 1st should put message into queue, 2nd should read from queue. Configure storing to disk, and compare queues performance.

## Launch containers
```bash
docker-compose up -d
```
Then launch redis cli of AOF redis container to turn on AOF data dyncsync:
```
docker-compose exec redis_reg_aof redis-cli

config get appendfsync
1) "appendfsync"
2) "everysec"
```
As we see, the configuration option is `everysec`. Let's set it to `always`:

```bash
config set appendfsync always
```

Now we can run the benchmark:
```bash
# asdf install
ruby benchmark.rb
```

## Results
```bash
                           user     system      total        real
Redis - AOF - publish    1.671212   1.245674   2.916886 ( 14.889575)
Redis - AOF - subscribe  0.000303   0.000142   0.000445 (  0.002278)

Redis - RDS - publish    1.662575   1.251716   2.914291 ( 15.186367)
Redis - RDS - subscribe  0.000129   0.000076   0.000205 (  0.002085)

Beanstalkd - publish     1.860068   0.973996   2.834064 ( 15.269413)
Beanstalkd - subscribe   3.664784   1.202894   4.867678 ( 43.402804)
```
We are interested in `total` column. As we see, for N=10 000 messages, the fastest is Beanstalkd. Though, it consumes the messages much more slowely in comparison to Redis. Redis consumes all the messages almost immidiately after they were published. But for Bnstalkd it takes significant amount to process the jobs. Redis RDS is slightly faster than Redis AOF.

## References
- [Beanstalk Console](https://github.com/ptrofimov/beanstalk_console)
- [Async Redis](https://github.com/socketry/async-redis)
- [Bitnami-redis params](https://github.com/bitnami/bitnami-docker-redis/blob/be7f79c3eb14e591dfe173b655d3edeb1acb1a10/5.0/debian-10/rootfs/opt/bitnami/scripts/redis-env.sh)
- [Redis PubSub Ruby Example](https://github.com/redis/redis-rb/blob/master/examples/pubsub.rb)
- [Redis Pub/Sub... How Does it Work?](https://thoughtbot.com/blog/redis-pub-sub-how-does-it-work)
- [Redis Persistance](https://redis.io/topics/persistence)
