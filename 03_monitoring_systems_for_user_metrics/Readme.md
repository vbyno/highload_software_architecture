# 03. Monitoring systems for user metrics

## Workflow
1) Visit [Marketing Platform](https://marketingplatform.google.com/) -> Admin; Create a new account

2) Create a Universal Analytics property (advanced settings)

Result:
![GA Property Created](img/ga_property_created.png)

3) `docker-compose up` for blog application (`shared/blog`)

Currency events are pushed to GA every minute
![Realtime Events](img/realtime_events.png)

## Links
- [Measurement Protocol](https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide)
- [How to create an account with TrackingID](https://www.youtube.com/watch?v=9yc0T4tR4gg)
- [GA Parameters](https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#ev)
