terraform {
  backend "s3" {
    bucket = "terraform-highload-state"
    key    = "24_aws_s3/live"
    region = "eu-west-3"
  }
}

provider "aws" {
  region = var.aws_region
}

module "aws_s3" {
  source = "../../shared/terraform_modules/s3"

  name_prefix = "highloadcourse"
  aws_region  = var.aws_region
  # log_prefix = "prefix"
}

# https://github.com/trussworks/terraform-aws-cloudtrail
module "aws_cloudtrail" {
  source = "../../shared/terraform_modules/cloudwatch_cloudtrail"

  s3_bucket_name     = module.aws_s3.log_bucket_name
  s3_target_bucket_name = module.aws_s3.bucket_name
  s3_key_prefix      = "prefix"
  log_retention_days = 90
}
