# 24. AWS. S3

## Assignment
Create a bucket where objects can’t be modified and all requests are logged.

## Create a bucket where objects can’t be modified and all requests are logged.
```bash
# if needed
# asdf plugin-add terraform https://github.com/asdf-community/asdf-hashicorp.git
cd ops

asdf install
```

Deploy an infrastructure with terraform

```bash
terraform init
terraform apply
```

It creates:
- S3 bucket `highloadcourse-bucket` – the main bucket to store the files. It is private, with object lock configuration being enabled.
- `highloadcourse-log-bucket` – to store logs.
- `cloudtrail` to log main bucket's Data events
- `cloudtrail-events` – a CloudWatch log group to observe the events delivered by `CloudTrail`
- all the needed roles and policies

<img src="img/cloudwatch_before_file_added.png" alt="" width="600"/>

There are no logs so far. Let's add an image to the bucket.

<img src="img/bucket_add_file.png" alt="" width="600"/>

Now let's check if there is any log on CloudWatch

<img src="img/cloudwatch_with_logs.png" alt="" width="600"/>

There is. Here is how it looks like:

<details>
  <summary>Click to expand</summary>

  ```json
  {
      "eventVersion": "1.08",
      "userIdentity": {
          "type": "AWSAccount",
          "principalId": "",
          "accountId": "ANONYMOUS_PRINCIPAL"
      },
      "eventTime": "2022-02-06T03:07:13Z",
      "eventSource": "s3.amazonaws.com",
      "eventName": "PreflightRequest",
      "awsRegion": "eu-west-3",
      "sourceIPAddress": "176.120.103.142",
      "userAgent": "[Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36]",
      "requestParameters": {
          "X-Amz-Date": "20220206T030712Z",
          "bucketName": "highloadcourse-bucket",
          "X-Amz-Algorithm": "AWS4-HMAC-SHA256",
          "X-Amz-SignedHeaders": "content-md5;content-type;host;x-amz-acl;x-amz-storage-class",
          "Host": "highloadcourse-bucket.s3.eu-west-3.amazonaws.com",
          "X-Amz-Expires": "300",
          "key": "img.png"
      },
      "responseElements": null,
      "additionalEventData": {
          "CipherSuite": "ECDHE-RSA-AES128-GCM-SHA256",
          "bytesTransferredIn": 0,
          "x-amz-id-2": "Bg7TiVGTKaedYosmpevfGtjGpgQ6POCOnHXnzVRC1N3SRw0c7yx/GWCmRAmAKaFiSOzbbo2EYQg=",
          "bytesTransferredOut": 0
      },
      "requestID": "N8AJPYZREDXR8D3F",
      "eventID": "5ff513cb-c982-438e-9fa0-9fb0a2334c90",
      "readOnly": true,
      "resources": [
          {
              "type": "AWS::S3::Object",
              "ARN": "arn:aws:s3:::highloadcourse-bucket/img.png"
          },
          {
              "accountId": "987045484890",
              "type": "AWS::S3::Bucket",
              "ARN": "arn:aws:s3:::highloadcourse-bucket"
          }
      ],
      "eventType": "AwsApiCall",
      "managementEvent": false,
      "recipientAccountId": "987045484890",
      "sharedEventID": "f15b1bb3-6d19-4f0a-b363-eab2e44218ea",
      "eventCategory": "Data"
  }
  ```
</details>

Now let's check what happens if we upload a new version of `img.png` file.

S3 allows us to add a new vesion of the file.

<img src="img/image_versions.png" alt="" width="600"/>

Though, it does not allow to delete bucket with terraform. Instead, iti displays an error:
```bash
Error: error S3 Bucket force_destroy: error deleting at least one object version, last error: AccessDenied deleting S3 Bucket (highloadcourse-bucket) Object (img.png) Version: sC2MMpBuMwoSUDD5CPdu.P0aTVyq0TDQ
```

```bash
terraform destroy
```
## References
- [S3 bucket with CloudTrail : terraform](https://github.com/tmknom/terraform-aws-s3-cloudtrail/blob/master/main.tf)

## Questions
1. Is it possible to log deletion? There are only `read` and `write`
2. The log seems to be uninformative. Is there any way to enrich it?
