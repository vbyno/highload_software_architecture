# 06. High Load Software Architecture

## Assignment
Choose proper architecture pattern for Instagram services.
Find what will be bottleneck in each case.
Find what might be a SPOF in each case.

## Solution

## Use Cases
Instagram has 4 main data resources:
- videos
- photos
- subscriptions
- likes, comments

Here are the main use cases:

<img src="img/use_cases.png" alt="" width="600"/>

## Components
Based on the Use-cases, we can split the functionality to multiple dependent components (services):

<img src="img/components.png" alt="" width="600"/>

Let's describe each of them.

### Posts
This is a core component. which links together multimedia files, likes and comments. This is read-havy component, therefore, its data must be cached.

*SPoF*: Cache Engine. Since for this component we heavily rely on caching, it could cause significant problems when the cache engine is off. To tackle this issue we will shard our cache, so that if one part fails, only a small part of posts are unavailable.

*Bottleneck*: Web-server and application server. To mitigate the rist of the servers not responding, we will build autoscaling based on number of concurrent requests.

### Photos/Videos Data Service
Photo and Video-services must be separated since they need different system resources to be processed. For splitting user experiences by their Geo-locations, CDN is needed.

*SPoF*: Since the save photos and videos on disk, the system won't work if the storage is down. Therefore, the storage must be Highly available.

*Bottleneck*: The services are read-intensive. There could be network delay issues when the users try to reach the content from the opposite side of the world. Moreover, heavy writes might undermine the reads consistency. Therefore, we separate writes and reads, add a storage replication, and utilize CDN cache.

Hence, the high-level architecture of this service looks like this:

<img src="img/multimedia_service.png" alt="" width="600"/>

### Likes
It is write-heavy component. We want to build it using append-only storage. Each record must contain post_id, user_id and timestamp and be unique for post-user pair.

*SPoF*: The database. The writes are frequent, but very small. We can choose distributed storage, like Cassandra, or column-based database as Riak to mitigate the risk of failure.

*Bottleneck*: Analytics can't be updated too frequently. Summary must be set once-in-time trough the window function and cached.

### Comments
The simple service, not such heavy as Likes.

*SPoF*: The database. It makes sense to have sharding for the comments.

*Bottleneck*: The database might not handle all the reeds.

### News feed
Business logic is built around the news feed. The system must display the best content to each particular user.
This component is compute-intensive. It will use previously trained ML model to calculate the relevant score for each post used have not seen.

*SPoF*: Application server algorithm. When it fails, no feed could be displayed.

*Bottleneck*: The popular accounts have many subsribers. It is hard to update 10+m news feeds every time some influencer adds a new post. Therefore, we will combine push-data with pull data for the news feed.

<img src="img/news_feed.png" alt="" width="600"/>

### Accounts / Followers
A simple service. Security and consistency are the most essential requirements for it. The service heavily rely on authorization service to provide tokens for the clients.

*SPoF*: Authorization service. Implement failover mechanizm for it.

*Bottleneck*: Database, since it must be consistent relational database.

### Search Engine
Its purpose is to search posts and accounts. Therefore, this service must contain the posts' and users' metadata. The most optimal way is to asyncroniously sync them via queue:

<img src="img/search_engine.png" alt="" width="600"/>

*SPoF*: The queue. Once it is down, the service does not receive new updates from the services. The que must be highly available.

*Bottleneck*: Memory. For the service to be robust, the engine cluster must shard the data and replicate the shards.Thanks to sharding we can have multiple small instances, rather thn few big ones.

## Summary
We've devided the system to services. Each service requires some specific type of resources, so they are easy to scale. We've denoted the SPoFs and bottlenecks of the components.

## References
- [Designing Instagram](http://highscalability.com/blog/2022/1/11/designing-instagram.html)
- [Instagram Architecture: 14 Million Users, Terabytes Of Photos, 100s Of Instances, Dozens Of Technologies](http://highscalability.com/blog/2011/12/6/instagram-architecture-14-million-users-terabytes-of-photos.html)
