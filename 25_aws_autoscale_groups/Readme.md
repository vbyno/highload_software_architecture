# 25. AWS. Autoscale groups

## Assignment
Create autoscale group that will contain one ondemand instance and will scale on spot instances.
Set up scaling policy based on AVG CPU usage
Set up scaling policy based on requests amount that allows non-linear growth

## Solution
Here is the current architecture:

<img src="img/architecture.png" alt="" width="600"/>

Target group connects load balancer and autoscaling group. Autoscaling group has exactly 1 on-demand instance. All the other instances are spot instances. We've set 4 types of desired spot instances: `t2.micro`/`t2.mini` and `t3.micro`/`t3.mini`.

Let's provision the infrastructure:

```bash
cd ops
terraform init
terraform apply
```

As we can see, since we've set 2 desired instances, there are 2 instances running. One is on-demand, while another one is spot instance.

<img src="img/two_instanses_running.png" alt="" width="600"/>


Check if instances are running
```bash
curl "$(terraform output -raw dns_name)"
<!DOCTYPE html><h2>Hello from AWS! []</h2>
```

However, in some time only one instance left running:

<img src="img/one_instanse_running.png" alt="" width="600"/>

In the terraform scripts, we've set up two scaling policies:

<img src="img/scaling_policies.png" alt="" width="600"/>

Let's use `siege` and find out if the auto scaling works:

```bash
siege -i -t1000s -c 20 "$(terraform output -raw dns_name)"
```

<img src="img/request_count_per_target.png" alt="" width="600"/>

At some point we've seen a new instance initializing:

<img src="img/scaling_one.png" alt="" width="600"/>

After one more minute +1 instance was initializing:

<img src="img/scaling_second.png" alt="" width="600"/>

```bash
terraform destroy
```

## References
- [Auto Scaling groups with multiple instance types and purchase options](https://docs.aws.amazon.com/autoscaling/ec2/userguide/ec2-auto-scaling-mixed-instances-groups.html)

## Questions
- Dig into scaling down, the percentage of the resources.
