terraform {
  backend "s3" {
    bucket = "terraform-highload-state"
    key    = "26_aws_lambda/live"
    region = "eu-west-3"
  }
}

provider "aws" {
  region = var.aws_region
}

data "http" "my_public_ip" {
  url = "http://ipv4.icanhazip.com"
}

data "aws_availability_zones" "available" {
  state = "available"
}

module "aws_vpc" {
  source = "../../shared/terraform_modules/vpc"

  name = "${var.name}-vpc"
  cidr_block = "10.1.0.0/16"
  availability_zones = data.aws_availability_zones.available.names
}

module "aws_security_group" {
  source = "../../shared/terraform_modules/security_group"

  name_prefix = var.name
  vpc_id      = module.aws_vpc.id
}

module "aws_lb_tagret_group" {
  source = "../../shared/terraform_modules/lb_tagret_group"

  name_prefix  = var.name
  vpc_id       = module.aws_vpc.id
}

module "aws_load_balancer" {
  source = "../../shared/terraform_modules/load_balancer"

  name_prefix              = var.name
  subnet_ids               = module.aws_vpc.subnet_ids
  ec2_instance_ids         = []
  autoscaling_group_ids    = [module.aws_autoscaler.autoscaling_group_id]
  assigned_security_groups = [module.aws_security_group.security_group_id]
  lb_target_group_arn      = module.aws_lb_tagret_group.lb_tagret_group_arn
}

module "aws_launch_template" {
  source = "../../shared/terraform_modules/launch_template"

  name_prefix              = var.name
  instance_type            = "t3.micro"
  ssh_local_key_path       = "~/.ssh/aws_key"
  vpc_id                   = module.aws_vpc.id
  my_public_ip             = chomp(data.http.my_public_ip.body)
  assigned_security_groups = [
    module.aws_security_group.security_group_id
  ]
}

module "aws_autoscaler" {
  source = "../../shared/terraform_modules/auto_scaler"

  name_prefix          = var.name
  desired_capacity     = 2
  scaling_policy       = "requests"
  min_size             = 1
  max_size             = 8
  subnet_ids           = module.aws_vpc.subnet_ids
  launch_template_id   = module.aws_launch_template.launch_template_id
  lb_target_group_arn  = module.aws_lb_tagret_group.lb_tagret_group_arn
  lb_target_group_arn_suffix = module.aws_lb_tagret_group.lb_tagret_group_arn_suffix
}
